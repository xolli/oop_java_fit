package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.LogManager;
import ru.nsu.fit.kazak.smartthread.SmartThread;

import javax.swing.*;

public class ViewerWaitedOrders extends SmartThread {
    private static final org.apache.logging.log4j.Logger logger = LogManager.getRootLogger();
    FactoryCar factory;
    JLabel label;

    public ViewerWaitedOrders(FactoryCar factory, JLabel label) {
        this.label = label;
        this.factory = factory;
    }

    @Override
    public void run() {
        while (true) {
            try {
                label.setText("Waited orders on factory: " + factory.getWaitedOrderCount());
                factory.waitChangeOrdersCount();
                if (!keepRunning()) {
                    break;
                }
            } catch (InterruptedException e) {
                logger.debug(Thread.currentThread().getName() + "interrupted");
                break;
            }
        }
    }
}
