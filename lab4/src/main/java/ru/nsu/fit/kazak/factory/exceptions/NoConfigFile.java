package ru.nsu.fit.kazak.factory.exceptions;

public class NoConfigFile extends RuntimeException {
    public NoConfigFile(String configFileName) {
        super("No setings file with name: " + configFileName);
    }
}
