package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.LogManager;
import ru.nsu.fit.kazak.smartthread.SmartThread;

import javax.swing.*;

public class ViewerWorkers extends SmartThread {
    private static final org.apache.logging.log4j.Logger logger = LogManager.getRootLogger();
    FactoryCar factory;
    JLabel label;

    public ViewerWorkers(FactoryCar factory, JLabel label) {
        this.label = label;
        this.factory = factory;
    }

    @Override
    public void run() {
        while (true) {
            try {
                label.setText("Workers on factory: " + factory.getCountWorkers());
                factory.waitChangeWorkersCount();
                if (!keepRunning()) {
                    break;
                }
            } catch (InterruptedException e) {
                logger.debug(Thread.currentThread().getName() + "interrupted");
                break;
            }
        }
    }
}
