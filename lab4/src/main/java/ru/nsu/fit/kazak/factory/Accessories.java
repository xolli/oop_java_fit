package ru.nsu.fit.kazak.factory;

public class Accessories extends PartMachine {
    public Accessories(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Accessory: " + id;
    }
}
