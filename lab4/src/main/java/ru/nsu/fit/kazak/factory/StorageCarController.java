package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.kazak.smartthread.SmartThread;

public class StorageCarController extends SmartThread {
    private static final Logger logger = LogManager.getRootLogger();
    private Storage<Car> storage;
    private FactoryCar factory;

    public StorageCarController(Storage<Car> storage, FactoryCar factory) {
        this.storage = storage;
        this.factory = factory;
        factory.getCars(storage.getCapacity() - storage.size());
    }

    @Override
    public void run() {
        while (true) {
            try {
                storage.waitGet();
                if (!keepRunning()) {
                    break;
                }
            } catch (InterruptedException e) {
                logger.debug(Thread.currentThread().getName() + " interrupted");
                break;
            }
            if (!storage.isFull()) {
                factory.getCars(1);
            }
        }
    }
}
