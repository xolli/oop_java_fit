package ru.nsu.fit.kazak.factory;

public class Car {
    private Body body;
    private Accessories accessories;
    private Motor motor;
    private Integer id;

    public Car(Body body, Accessories accessories, Motor motor, int id) {
        this.body = body;
        this.accessories = accessories;
        this.motor = motor;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Auto " + id + " (" + body + ", " + motor + "," + accessories + ")";
    }
}
