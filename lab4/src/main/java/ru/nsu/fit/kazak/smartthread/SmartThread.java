package ru.nsu.fit.kazak.smartthread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Can monitor object and be in ThreadPool
 */
public class SmartThread extends Thread {
    private static final Logger logger = LogManager.getLogger();
    private ThreadState state = ThreadState.RUNNING;
    private final Object monitor = new Object();

    public SmartThread() {
        super();
    }

    public SmartThread(String name) {
        super(name);
    }

    public void smartStop() {
        synchronized (monitor) {
            state = ThreadState.STOP;
            monitor.notifyAll();
        }
    }

    public void smartSuspend () {
        synchronized (monitor) {
            if (state != ThreadState.STOP) {
                state = ThreadState.SLEEP;
            }
        }
    }

    public void smartResume() {
        synchronized (monitor) {
            if (state == ThreadState.SLEEP) {
                state = ThreadState.RUNNING;
                monitor.notifyAll();
            }
        }
    }

    protected boolean keepRunning() {
        synchronized (monitor) {
            if (isInterrupted()) {
                logger.debug(Thread.currentThread().getName() + "interrupted");
                return false;
            }
            if (state == ThreadState.RUNNING) {
                return true;
            }
            else {
                while (true) {
                    if (isInterrupted()) {
                        logger.debug(Thread.currentThread().getName() + "interrupted");
                        return false;
                    } else if (state == ThreadState.STOP) {
                        return false;
                    } else if (state == ThreadState.SLEEP) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            logger.debug(Thread.currentThread().getName() + "interrupted");
                            return false;
                        }
                    } else if (state == ThreadState.RUNNING) {
                        return true;
                    }
                }
            }
        }
    }
}
