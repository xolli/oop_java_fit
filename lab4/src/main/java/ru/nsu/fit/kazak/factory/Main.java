package ru.nsu.fit.kazak.factory;

public class Main {
    public static void main(String[] args) {
        ProductionCycle productionCycle = new ProductionCycle();
        productionCycle.run();
    }
}
