package ru.nsu.fit.kazak.factory;

public class Body extends PartMachine {

    public Body(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Body: " + id;
    }
}
