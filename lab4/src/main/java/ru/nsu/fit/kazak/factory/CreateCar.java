package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.kazak.threadpool.Task;

import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

public class CreateCar implements Task {
    private static final Logger logger = LogManager.getRootLogger();
    private Storage<Car> storageCar;
    private Storage<Accessories> storageAccessories;
    private Storage<Body> storageBody;
    private Storage<Motor> storageMotor;
    private static final AtomicInteger numberGenerator = new AtomicInteger(0);
    private Accessories newAccessories;
    private Body newBody;
    private Motor newMotor;

    public CreateCar (Storage<Accessories> storageAccessories, Storage<Body> storageBody, Storage<Motor> StorageMotor, Storage<Car> storageCar) {
        this.storageCar = storageCar;
        this.storageAccessories = storageAccessories;
        this.storageBody = storageBody;
        this.storageMotor = StorageMotor;
        this.newAccessories = null;
        this.newBody = null;
        this.newMotor = null;
    }

    @Override
    public String getName() {
        return "Create Car";
    }

    @Override
    public void performWork() throws InterruptedException {
        try {
            newAccessories = storageAccessories.get();
            newBody = storageBody.get();
            newMotor = storageMotor.get();
            sleep(2000);
        } catch (InterruptedException e) {
            stop();
            throw new InterruptedException();
        }
        int number = numberGenerator.incrementAndGet();
        storageCar.put(new Car(newBody, newAccessories, newMotor, number));
        logger.debug(Thread.currentThread().getName() + " create car");
    }

    @Override
    public void stop() throws InterruptedException {
        if (newAccessories != null && !storageAccessories.isFull()) {
            storageAccessories.put(newAccessories, 1);
        }
        if (newBody != null && !storageBody.isFull()) {
            storageBody.put(newBody, 1);
        }
        if (newMotor != null && !storageBody.isFull()) {
            storageMotor.put(newMotor, 1);
        }
    }
}
