package ru.nsu.fit.kazak.factory;

import ru.nsu.fit.kazak.smartthread.SmartThread;
import org.apache.logging.log4j.*;

public class Dealer extends SmartThread {
    private static final Logger logger = LogManager.getRootLogger();
    Storage<Car> storage;
    long pause;
    Integer id;
    private final Object alarm = new Object();
    boolean awakened;

    public Dealer(Storage<Car> storage, long pause, int id) {
        this.storage = storage;
        this.pause = pause;
        this.id = id;
    }

    public void run() {
        while (true) {
            try {
                synchronized (alarm) {
                    alarm.wait(pause * 1000);
                }
                if (awakened) {
                    awakened = false;
                    continue;
                }
                Car newCar = storage.get();
                logger.info(toString() + ": " + newCar);
                if (!keepRunning()) {
                    break;
                }
            } catch (InterruptedException e) {
                logger.debug(Thread.currentThread().getName() + "interrupted");
            }
        }
    }

    public void setPause (int newPause) {
        this.pause = newPause;
    }

    @Override
    public String toString() {
        return "Dealer " + id;
    }

    public long getPause() {
        return pause;
    }

    public void wakeUp() {
        synchronized (alarm) {
            awakened = true;
            alarm.notifyAll();
        }
    }
}
