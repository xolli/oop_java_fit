package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.kazak.smartthread.SmartThread;

import java.util.concurrent.atomic.AtomicInteger;

public class CreatorAccessories extends SmartThread implements Creator {
    private static final Logger logger = LogManager.getRootLogger();
    private Storage<Accessories> storage;
    private long pause;
    private static final AtomicInteger numberGenerator = new AtomicInteger(0);
    private Object alarm;
    boolean awakened;

    public CreatorAccessories(Storage<Accessories> storage, long pause) {
        this.storage = storage;
        this.pause = pause;
        this.alarm = new Object();
        this.awakened = false;
    }

    @Override
    public void run() {
        int number = 0;
        while (true) {
            try {
                synchronized (alarm) {
                    alarm.wait(pause * 1000);
                }
                if (awakened) {
                    awakened = false;
                    continue;
                }
                number = numberGenerator.incrementAndGet();
                storage.put(new Accessories(number));
                logger.debug(Thread.currentThread().getName() + " create accessory");
                if (!keepRunning()) {
                    break;
                }
            } catch (InterruptedException ex) {
                logger.debug(Thread.currentThread().getName() + "interrupted");
                break;
            } catch (IllegalMonitorStateException e) {
                logger.debug("IllegalMonitorStateException");
                break;
            } catch (IllegalArgumentException e) {
                logger.debug("IllegalArgumentException");
                break;
            }
        }
    }

    @Override
    public void setPause(long newPause) {
        pause = newPause;
    }

    @Override
    public long getPause() {
        return pause;
    }

    @Override
    public void wakeUp() {
        synchronized (alarm) {
            awakened = true;
            alarm.notifyAll();
        }
    }
}
