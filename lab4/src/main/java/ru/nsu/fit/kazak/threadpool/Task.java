package ru.nsu.fit.kazak.threadpool;

public interface Task {
    String getName();
    void performWork() throws InterruptedException;
    public void stop() throws InterruptedException;
}
