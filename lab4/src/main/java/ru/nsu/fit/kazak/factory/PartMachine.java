package ru.nsu.fit.kazak.factory;

public abstract class PartMachine {
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public PartMachine (int id) {
        this.id = id;
    }
}
