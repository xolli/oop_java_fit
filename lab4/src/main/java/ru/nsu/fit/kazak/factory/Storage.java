package ru.nsu.fit.kazak.factory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.*;

public class Storage <T> extends HashSet<T> {
    private int capacity;
    final Lock lock = new ReentrantLock();
    final Condition notFull  = lock.newCondition();
    final Condition notEmpty = lock.newCondition();
    final Condition changeState = lock.newCondition();
    private Integer countDetail;

    public Storage(int capacity) {
        super();
        this.capacity = capacity;
        this.countDetail = 0;
    }

    public void put(T newObject, long timeout) throws InterruptedException {
        lock.lock();
        try {
            while (size() == capacity) {
                notFull.await(timeout, TimeUnit.MILLISECONDS);
            }
            add(newObject);
            ++countDetail;
            notEmpty.signalAll();
            changeState.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public void put (T newObject) throws InterruptedException {
        put(newObject, 0);
    }

    public T get() throws InterruptedException {
        lock.lock();
        try {
            while (isEmpty()) {
                notEmpty.await();
            }
            notFull.signalAll();
            changeState.signalAll();
            Iterator<T> it = iterator();
            T ret = it.next();
            remove(ret);
            return ret;
        } finally {
            lock.unlock();
        }
    }

    public boolean isFull() {
        return size() == capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void waitPut() throws InterruptedException {
        lock.lock();
        notEmpty.await();
        lock.unlock();
    }

    public void waitGet() throws InterruptedException {
        lock.lock();
        notFull.await();
        lock.unlock();
    }

    public void waitChangeState() throws InterruptedException {
        lock.lock();
        changeState.await();
        lock.unlock();
    }

    public Integer getCountDetail() {
        return countDetail;
    }
}
