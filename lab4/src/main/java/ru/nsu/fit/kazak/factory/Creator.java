package ru.nsu.fit.kazak.factory;

public interface Creator {
    public void setPause(long newPause);

    public long getPause();

    public void wakeUp();
}
