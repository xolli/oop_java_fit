package ru.nsu.fit.kazak.threadpool.exceptions;

public class EmptyPoolThread extends RuntimeException {
    public EmptyPoolThread() {
        super("Empty Pool Thread");
    }
}
