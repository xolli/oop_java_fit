package ru.nsu.fit.kazak.threadpool;

import org.apache.logging.log4j.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

class PooledThread extends Thread{
    private static final Logger logger = LogManager.getLogger();
    private List<ThreadPoolTask> tasks;
    private final AtomicInteger countExecutingTasks;

    PooledThread(String name, List<ThreadPoolTask> tasks, AtomicInteger countExecutingTasks) {
        super(name);
        this.tasks = tasks;
        this.countExecutingTasks = countExecutingTasks;
    }

    private void performTask(ThreadPoolTask task) throws InterruptedException {
        synchronized (countExecutingTasks) {
            countExecutingTasks.incrementAndGet();
            countExecutingTasks.notifyAll();
        }
        task.prepare();
        try {
            task.go();
        }
        catch (InterruptedException ex) {
            task.stop();
            synchronized (tasks) {
                tasks.add(task);
                tasks.notifyAll();
            }
            throw new InterruptedException();
        } finally {
            synchronized (countExecutingTasks) {
                countExecutingTasks.decrementAndGet();
                countExecutingTasks.notifyAll();
            }
        }
        task.finish();
    }

    @Override
    public void run() {
        while (true)
        {
            ThreadPoolTask task;
            synchronized (tasks)
            {
                if (tasks.isEmpty())
                {
                    try {
                        tasks.wait();
                    }
                    catch (InterruptedException ex) {
                        break;
                    }
                    continue;
                } else {
                    task = tasks.remove(0);
                    tasks.notifyAll();
                }
            }
            try {
                performTask(task);
            } catch (InterruptedException e) {
                logger.debug("Thread" + getName() + " interrupted");
                break;
            }
        }
    }
}
