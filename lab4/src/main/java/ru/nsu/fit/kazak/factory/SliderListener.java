package ru.nsu.fit.kazak.factory;

public interface SliderListener {
    void reportChange(Integer newValue);
}
