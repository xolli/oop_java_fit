package ru.nsu.fit.kazak.factory;

import ru.nsu.fit.kazak.threadpool.ThreadPool;

public class FactoryCar {
    private ThreadPool workers;
    private Storage<Car> storageCar;
    private Storage<Accessories> storageAccessories;
    private Storage<Body> storageBody;
    private Storage<Motor> storageMotor;

    public FactoryCar(Storage<Accessories> storageAccessories, Storage<Body> storageBody, Storage<Motor> storageMotor, Storage<Car> storageCar, int countWorkers) {
        this.storageCar = storageCar;
        this.storageAccessories = storageAccessories;
        this.storageBody = storageBody;
        this.storageMotor = storageMotor;
        workers = new ThreadPool(countWorkers);
    }

    public void getCars(int numberCars) {
        for (int i = 0; i < numberCars; ++i) {
            workers.addTask(new CreateCar(storageAccessories, storageBody, storageMotor, storageCar));
        }
    }

    public int getWaitedOrderCount() {
        return workers.getWaitTasksCount();
    }

    public void waitChangeOrdersCount() throws InterruptedException {
        workers.waitChangeWaitedTasksCount();
    }

    public void close() {
        workers.shutdown();
    }

    public void setCountWorkers(int newValue) {
        workers.setSize(newValue);
    }

    public int getCountWorkers() {
        return workers.getCountThreads();
    }

    public void waitChangeWorkersCount() throws InterruptedException {
        workers.waitChangeThreadsCount();
    }

    public int getCountExecutingOrders() {
        return workers.getExecutingTasksCount();
    }

    public void waitChangeExecutingOrders() throws InterruptedException {
        workers.waitChangeExecutingTasksCount();
    }
}
