package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.LoggerConfig;
import ru.nsu.fit.kazak.factory.exceptions.NoConfigFile;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class ProductionCycle implements WindowListener {
    private static final Logger logger = LogManager.getRootLogger();
    private Set<CreatorAccessories> creatorsAccessories;
    private Set<Dealer> dealers;
    private CreatorBody creatorBody;
    private CreatorMotor creatorMotor;
    private FactoryCar factory;
    private StorageCarController controller;
    private Storage<Body> storageBody;
    private Storage<Motor> storageMotor;
    private Storage<Accessories> storageAccessories;
    private Storage<Car> storageCar;
    ProductionViewer viewer;

    public ProductionCycle() throws NoConfigFile {
        loadOptions();
    }

    private void loadOptions() throws NoConfigFile {
        Properties configs = new Properties();
        String configFile = "config.properties";
        InputStream fin = getClass().getResourceAsStream(configFile);
        try {
            configs.load(fin);
        } catch (IOException e) {
            throw new NoConfigFile(configFile);
        }
        if (configs.getProperty("Logging").equals("true")){
            Configurator.setRootLevel(Level.INFO);
        } else {
            Configurator.setRootLevel(Level.ERROR);
            Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ERROR);
        }
        int storageBodyCapacity = Integer.parseInt(configs.getProperty("StorageBodyCapacity"));
        int storageMotorCapacity = Integer.parseInt(configs.getProperty("StorageMotorCapacity"));
        int storageAccessoryCapacity = Integer.parseInt(configs.getProperty("StorageAccessoryCapacity"));
        int storageAutoCapacity = Integer.parseInt(configs.getProperty("StorageAutoCapacity"));
        int accessoryCreators = Integer.parseInt(configs.getProperty("AccessoryCreators"));
        int workersCount = Integer.parseInt(configs.getProperty("WorkersCount"));
        int dealersCount = Integer.parseInt(configs.getProperty("DealersCount"));
        long pauseCreatorBodyDefault = Long.parseLong(configs.getProperty("PauseCreatorBodyDefault"));
        long pauseCreatorMotorDefault = Long.parseLong(configs.getProperty("PauseCreatorMotorDefault"));
        long pauseCreatorAccessoryDefault = Long.parseLong(configs.getProperty("PauseCreatorAccessoryDefault"));
        long pauseDealerDefault = Long.parseLong(configs.getProperty("PauseDealerDefault"));
        storageBody = new Storage<>(storageBodyCapacity);
        creatorBody = new CreatorBody(storageBody, pauseCreatorBodyDefault);
        storageMotor = new Storage<>(storageMotorCapacity);
        creatorMotor = new CreatorMotor(storageMotor, pauseCreatorMotorDefault);
        storageAccessories = new Storage<>(storageAccessoryCapacity);
        creatorsAccessories = new HashSet<>(accessoryCreators);
        for (int i = 0; i < accessoryCreators; ++i) {
            creatorsAccessories.add(new CreatorAccessories(storageAccessories, pauseCreatorAccessoryDefault));
        }
        storageCar = new Storage<>(storageAutoCapacity);
        factory = new FactoryCar(storageAccessories, storageBody, storageMotor, storageCar, workersCount);
        dealers = new HashSet<>(dealersCount);
        for (int i = 1; i <= dealersCount; ++i) {
            dealers.add(new Dealer(storageCar, pauseDealerDefault, i));
        }
        controller = new StorageCarController(storageCar, factory);
    }

    public void run() {
        creatorBody.start();
        creatorMotor.start();
        for (CreatorAccessories creator : creatorsAccessories) {
            creator.start();
        }
        for (Dealer dealer : dealers) {
            dealer.start();
        }
        controller.start();

        viewer = new ProductionViewer(creatorBody, creatorMotor, creatorsAccessories, dealers,
                                                       storageBody, storageMotor, storageAccessories, storageCar, factory);
        viewer.setVisible(true);
        viewer.addWindowListener(this);
    }

    public void stop() {
        creatorBody.smartStop();
        try {
            creatorBody.join();
        } catch (InterruptedException e) {
            logger.debug(creatorBody.getName() + " interrupted");
        }
        creatorMotor.smartStop();
        try {
            creatorBody.join();
        } catch (InterruptedException e) {
            logger.debug(creatorMotor.getName() + " interrupted");
        }
        for (CreatorAccessories creator : creatorsAccessories) {
            creator.smartStop();
            try {
                creator.join(50);
            } catch (InterruptedException e) {
                logger.debug(creator.getName() + " interrupted");
            }
        }
        for (Dealer dealer : dealers) {
            dealer.smartStop();
            try {
                dealer.join(50);
            } catch (InterruptedException e) {
                logger.debug(dealer.getName() + " interrupted");
            }
        }
        controller.smartStop();
        try {
            controller.join(500);
        } catch (InterruptedException e) {
            logger.debug(controller.getName() + " interrupted");
        }
        factory.close();
    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        viewer.stop();
        stop();
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }
}
