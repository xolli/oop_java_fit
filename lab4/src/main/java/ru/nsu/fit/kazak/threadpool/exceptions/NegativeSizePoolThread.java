package ru.nsu.fit.kazak.threadpool.exceptions;

public class NegativeSizePoolThread extends RuntimeException {
    public NegativeSizePoolThread() {
        super("Negative size pool thread");
    }
}
