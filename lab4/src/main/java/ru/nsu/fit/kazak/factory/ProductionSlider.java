package ru.nsu.fit.kazak.factory;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

class ChangeInt implements ChangeListener {
    SliderListener listener;

    ChangeInt(SliderListener listener) {
        this.listener = listener;
    }

    @Override
    synchronized public void stateChanged(ChangeEvent event) {
        listener.reportChange(((JSlider)event.getSource()).getValue());
    }
}

public class ProductionSlider extends JSlider {
    public ProductionSlider(SliderListener listener, long defaultValye, long minValue, long maxValue) {
        super(new DefaultBoundedRangeModel((int)defaultValye, 0, (int)minValue, (int)maxValue));
        setPaintLabels(true);
        setMajorTickSpacing(5);
        addChangeListener(new ChangeInt(listener));
    }
}
