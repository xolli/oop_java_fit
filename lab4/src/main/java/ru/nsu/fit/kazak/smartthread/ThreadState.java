package ru.nsu.fit.kazak.smartthread;

public enum ThreadState {
    RUNNING,
    SLEEP,
    STOP;
}
