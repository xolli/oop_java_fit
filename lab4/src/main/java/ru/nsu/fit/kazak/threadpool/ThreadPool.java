package ru.nsu.fit.kazak.threadpool;

import org.apache.logging.log4j.*;
import ru.nsu.fit.kazak.threadpool.exceptions.EmptyPoolThread;
import ru.nsu.fit.kazak.threadpool.exceptions.NegativeSizePoolThread;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPool implements TaskListener {
    private static final Logger logger = LogManager.getLogger();
    private final Set<PooledThread> threads = new HashSet<>();
    private final List<ThreadPoolTask> tasks;
    private final AtomicInteger executingTasks;

    public ThreadPool(int threadCount) {
        tasks = new LinkedList<>();
        executingTasks = new AtomicInteger(0);
        for (int i = 1; i <= threadCount; ++i) {
            threads.add(new PooledThread("Thread number " + i, tasks, executingTasks));
        }
        for (PooledThread thread : threads) {
            thread.start();
        }
    }

    public void shutdown() {
        for (PooledThread thread : threads) {
            thread.interrupt();
        }
        for (PooledThread thread : threads) {
            try {
                thread.join(100);
            } catch (InterruptedException e) {
                logger.debug(thread.getName() + " interrupted");
            }
        }
        logger.debug("Shutdown");
    }

    public void decThread() {
        synchronized (threads) {
            if (threads.isEmpty()) {
                throw new EmptyPoolThread();
            }
            PooledThread removedThread = threads.iterator().next();
            removedThread.interrupt();
            try {
                removedThread.join(1);
            } catch (InterruptedException e) {
                logger.debug(removedThread.getName() + " inrettupted");
            }
            threads.remove(removedThread);
            threads.notifyAll();
        }
    }

    public void incThread() {
        PooledThread newThread = new PooledThread("Thread number " + threads.size(), tasks, executingTasks);
        synchronized (threads) {
            threads.add(newThread);
            threads.notifyAll();
        }
        newThread.start();
    }

    public void setSize(int newSize) {
        if (newSize < 0) {
            throw new NegativeSizePoolThread();
        }
        if (newSize < threads.size()) {
            while (newSize != threads.size()) {
                decThread();
            }
        } else if (newSize > threads.size()) {
            while (newSize != threads.size()) {
                incThread();
            }
        }
    }

    public void addTask(Task task) {
        synchronized (tasks) {
            tasks.add(new ThreadPoolTask(task, this));
            tasks.notifyAll();
        }
    }

    public int getWaitTasksCount() {
        return tasks.size();
    }

    @Override
    public void taskInterrupted(Task task) {
        logger.debug("Interrupted " + task);
    }

    @Override
    public void taskFinished(Task task) {
        logger.debug("Finished " + task);
    }

    @Override
    public void taskStarted(Task task) {
        logger.debug("Started " + task);
    }

    public void waitChangeWaitedTasksCount() throws InterruptedException {
        synchronized (tasks) {
            tasks.wait();
        }
    }

    public int getCountThreads() {
        return threads.size();
    }

    public void waitChangeThreadsCount() throws InterruptedException {
        synchronized (threads) {
            threads.wait();
        }
    }

    public void waitChangeExecutingTasksCount() throws InterruptedException {
        synchronized (executingTasks) {
            executingTasks.wait();
        }
    }

    public int getExecutingTasksCount() {
        return executingTasks.get();
    }
}
