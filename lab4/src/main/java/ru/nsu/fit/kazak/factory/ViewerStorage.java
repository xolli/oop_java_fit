package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.LogManager;
import ru.nsu.fit.kazak.smartthread.SmartThread;

import javax.swing.*;

public class ViewerStorage extends SmartThread {
    private static final org.apache.logging.log4j.Logger logger = LogManager.getRootLogger();
    Storage storage;
    JLabel label;
    String nameStorage;

    public ViewerStorage(Storage storage, JLabel label, String nameStorage) {
        this.storage = storage;
        this.label = label;
        this.nameStorage = nameStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                label.setText("Size storage " + nameStorage + ": " + storage.size() + "              Count created " + nameStorage + ": " + storage.getCountDetail());
                storage.waitChangeState();
                if (!keepRunning()) {
                    break;
                }
            } catch (InterruptedException e) {
                logger.debug(Thread.currentThread().getName() + "interrupted");
                break;
            }
        }
    }
}
