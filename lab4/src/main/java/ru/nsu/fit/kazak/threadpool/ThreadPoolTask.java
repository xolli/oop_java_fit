package ru.nsu.fit.kazak.threadpool;

class ThreadPoolTask {
    private TaskListener listener;
    Task task;

    public ThreadPoolTask(Task task, TaskListener listener) {
        this.task = task;
        this.listener = listener;
    }

    void go() throws InterruptedException {
        task.performWork();
    }

    String getName() {
        return task.getName();
    }

    void prepare() {
        listener.taskStarted(task);
    }

    void finish () {
        listener.taskFinished(task);
    }

    void interrupt() {
        listener.taskInterrupted(task);
    }

    void stop() throws InterruptedException {
        task.stop();
    }
}
