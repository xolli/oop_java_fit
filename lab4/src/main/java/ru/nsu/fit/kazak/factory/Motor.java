package ru.nsu.fit.kazak.factory;

public class Motor extends PartMachine {

    public Motor(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Motor: " + id;
    }
}
