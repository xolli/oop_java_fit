package ru.nsu.fit.kazak.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.util.Set;

public class ProductionViewer extends JFrame {
    private static final Logger logger = LogManager.getRootLogger();
    private JPanel content;
    private ViewerStorage viewerBodyStorage;
    private ViewerStorage viewerMotorStorage;
    private ViewerStorage viewerAccessoriesStorage;
    private ViewerStorage viewerCarStorage;
    private ViewerWaitedOrders viewerWaitedOrders;
    private ViewerExecutingOrders viewerExecutingOrders;
    private ViewerWorkers viewerWorkers;

    public ProductionViewer(CreatorBody creatorBody, CreatorMotor creatorMotor, Set<CreatorAccessories> creatorsAccessories,
                            Set<Dealer> dealers,
                            Storage<Body> storageBody, Storage<Motor> storageMotor, Storage<Accessories> storageAccessories,
                            Storage<Car> storageCar, FactoryCar factory) {
        super("Production");
        setDefaultCloseOperation (JFrame.DO_NOTHING_ON_CLOSE);
        setSize(new Dimension(600, 800));
        setResizable(false);
        JLabel pauseBody = new JLabel("Body pause: " + creatorBody.getPause());
        ProductionSlider sliderBody = new ProductionSlider(new SliderListener() {
            @Override
            public void reportChange(Integer newValue) {
                creatorBody.setPause(newValue);
                creatorBody.wakeUp();
                pauseBody.setText("Body pause: " + newValue);
            }
        }, creatorBody.getPause(), 1, 60);
        JLabel pauseMotor = new JLabel("Motor pause: " + creatorMotor.getPause());
        ProductionSlider sliderMotor = new ProductionSlider(new SliderListener() {
            @Override
            public void reportChange(Integer newValue) {
                creatorMotor.setPause(newValue);
                creatorMotor.wakeUp();
                pauseMotor.setText("Motor pause: " + newValue);
            }
        }, creatorMotor.getPause(), 1, creatorMotor.getPause() + 50);
        CreatorAccessories firstCreatorAccessory = creatorsAccessories.iterator().next();
        JLabel pauseAccessories = new JLabel("Accessories pause: " + firstCreatorAccessory.getPause());
        ProductionSlider sliderAccessories = new ProductionSlider(new SliderListener() {
            @Override
            public void reportChange(Integer newValue) {
                for (CreatorAccessories creator : creatorsAccessories) {
                    creator.setPause(newValue);
                    creator.wakeUp();
                }
                pauseAccessories.setText("Accessories pause: " + newValue);
            }
        }, firstCreatorAccessory.getPause(), 1, firstCreatorAccessory.getPause() + 50);
        Dealer firstDealer = dealers.iterator().next();
        JLabel pauseDealers = new JLabel("Dealers pause: " + firstDealer.getPause());
        ProductionSlider sliderDealers = new ProductionSlider(new SliderListener() {
            @Override
            public void reportChange(Integer newValue) {
                for (Dealer dealer : dealers) {
                    dealer.setPause(newValue);
                    dealer.wakeUp();
                }
                pauseDealers.setText("Dealers pause: " + newValue);
            }
        }, firstDealer.getPause(), 1, firstDealer.getPause() + 50);
        ProductionSlider sliderWorkers = new ProductionSlider(new SliderListener() {
            @Override
            public void reportChange(Integer newValue) {
                factory.setCountWorkers(newValue);
            }
        }, factory.getCountWorkers(), 0, factory.getCountWorkers() + 50);
        JLabel sizeStorageBody = new JLabel("Size Storage Body: " + storageBody.size());
        JLabel sizeStorageMotor = new JLabel("Size Storage Motor: " + storageMotor.size());
        JLabel sizeStorageAccessories = new JLabel("Size Storage Accessories: " + storageAccessories.size());
        JLabel sizeStorageCar = new JLabel("Size Storage Car: " + storageCar.size());
        JLabel waitOrders = new JLabel("Waited orders on factory: " + factory.getWaitedOrderCount());
        JLabel waitExecOrders = new JLabel("Executing orders on factory: " + factory.getCountExecutingOrders());
        JLabel waitWorkers = new JLabel("Workers on factory: " + factory.getCountWorkers());
        viewerBodyStorage = new ViewerStorage(storageBody, sizeStorageBody, "Body");
        viewerMotorStorage = new ViewerStorage(storageMotor, sizeStorageMotor, "Motor");
        viewerAccessoriesStorage = new ViewerStorage(storageAccessories, sizeStorageAccessories, "Accessories");
        viewerCarStorage = new ViewerStorage(storageCar, sizeStorageCar, "Car");
        viewerWaitedOrders = new ViewerWaitedOrders(factory, waitOrders);
        viewerExecutingOrders = new ViewerExecutingOrders(factory, waitExecOrders);
        viewerWorkers = new ViewerWorkers(factory, waitWorkers);
        viewerBodyStorage.start();
        viewerMotorStorage.start();
        viewerAccessoriesStorage.start();
        viewerCarStorage.start();
        viewerWaitedOrders.start();
        viewerExecutingOrders.start();
        viewerWorkers.start();
        content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        content.add(pauseBody);
        content.add(sliderBody);
        content.add(pauseMotor);
        content.add(sliderMotor);
        content.add(pauseAccessories);
        content.add(sliderAccessories);
        content.add(pauseDealers);
        content.add(sliderDealers);
        content.add(waitWorkers);
        content.add(sliderWorkers);
        content.add(sizeStorageBody);
        content.add(sizeStorageMotor);
        content.add(sizeStorageAccessories);
        content.add(sizeStorageCar);
        content.add(waitOrders);
        content.add(waitExecOrders);
        getContentPane().add(content);
    }

    public void stop() {
        viewerBodyStorage.smartStop();
        try {
            viewerBodyStorage.join(100);
        } catch (InterruptedException e) {
            logger.debug(viewerBodyStorage.getName() + " interrupted");
        }
        viewerMotorStorage.smartStop();
        try {
            viewerMotorStorage.join(100);
        } catch (InterruptedException e) {
            logger.debug(viewerMotorStorage.getName() + " interrupted");
        }
        viewerAccessoriesStorage.smartStop();
        try {
            viewerAccessoriesStorage.join(100);
        } catch (InterruptedException e) {
            logger.debug(viewerAccessoriesStorage.getName() + " interrupted");
        }
        viewerCarStorage.smartStop();
        try {
            viewerCarStorage.join(100);
        } catch (InterruptedException e) {
            logger.debug(viewerCarStorage.getName() + " interrupted");
        }
        viewerWaitedOrders.smartStop();
        try {
            viewerWaitedOrders.join(100);
        } catch (InterruptedException e) {
            logger.debug(viewerWaitedOrders.getName() + " interrupted");
        }
        setVisible(false);
        dispose();
    }
}
