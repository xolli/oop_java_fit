package ru.nsu.fit.kazak.arkanoid;


import java.io.Serializable;

public class InfoPlayer implements Serializable {
    private static final long serialVersionUID = 1L;
    private final int WIDTH_SCREEN = 600;
    private final int HEIGHT_SCREEN = 800;
    private int myX;
    private final int myY;
    private boolean isConnected;
    private final boolean isAttachedBall;

    public InfoPlayer(Player player, boolean isConnected) {
        this.myX = player.getPos().getX();
        this.myY = player.getPos().getY();
        this.isAttachedBall = player.isAttachedBall();
        this.isConnected = isConnected;
    }

    public InfoPlayer(Player player) {
        this.myX = player.getPos().getX();
        this.myY = player.getPos().getY();
        this.isAttachedBall = player.isAttachedBall();
        this.isConnected = true;
    }

    public int getMyX() {
        return myX;
    }

    public int getMyY() {
        return myY;
    }

    public boolean isAttachedBall() {
        return isAttachedBall;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void invert() {
        myX = WIDTH_SCREEN - myX - 70;
    }
}
