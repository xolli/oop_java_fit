package ru.nsu.fit.kazak.arkanoid;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Player extends Sprite {
    private final int widthPlayer, heightPlayer;
    private final int widthScreen;
    private boolean isAttachedBall;
    private Ball attachedBall;
    private int number;

    public Player(int widthScreen, int heightScreen, Coord coord, int number) {
        this.widthScreen = widthScreen;
        isAttachedBall = false;
        widthPlayer = 70;
        heightPlayer = 20;
        this.pos.setX(coord.getX());
        this.pos.setY(coord.getY());
        figure = new Rectangle2D.Double(coord.getX(), coord.getY(), widthPlayer, heightPlayer);
        this.number = number;
    }

    public void attachBall(Ball ball) {
        isAttachedBall = true;
        attachedBall = ball;
        moveStickedBall();
    }

    public void untieBound() {
        if (isAttachedBall) {
            isAttachedBall = false;
            attachedBall.setSpeed(0.5);
        }
    }

    public void setCoord(Double x, Double y) {
        x += widthPlayer / 2;
        if (((isAttachedBall && (x + attachedBall.getDiameter() / 2 < widthScreen) && (x - attachedBall.getDiameter() / 2) > 0) ||
                (!isAttachedBall && x < widthScreen && x > 0)) && number == 1) {
            synchronized (figure) {
                ((Rectangle.Double)figure).x = x - (double)widthPlayer / 2;
            }
        } else if ((isAttachedBall && (x - attachedBall.getDiameter() / 2 > 0) && (x + attachedBall.getDiameter() / 2 < widthScreen) ||
                   (!isAttachedBall && x > 0 && x < widthScreen)) && number == 2) {
            synchronized (figure) {
                ((Rectangle.Double)figure).x = x - (double)widthPlayer / 2;
            }

        }
        synchronized (pos) {
            pos.setX((int)((Rectangle.Double)figure).x);
            pos.setY((int)((Rectangle.Double)figure).y);
        }
        moveStickedBall();
    }

    public void moveStickedBall() {
        double myX = ((Rectangle2D.Double) figure).x;
        double myY = ((Rectangle2D.Double) figure).y;
        if (isAttachedBall && number == 1) {
            synchronized (attachedBall) {
                attachedBall.setCoord(myX + (widthPlayer / 2) - (attachedBall.getDiameter() / 2), myY - attachedBall.getDiameter());
            }
        } else if (isAttachedBall && number == 2) {
            synchronized (attachedBall) {
                attachedBall.setCoord(myX + (widthPlayer / 2) - (attachedBall.getDiameter() / 2), myY + heightPlayer);
            }
        }
    }

    public boolean isAttachedBall() {
        return isAttachedBall;
    }

    public void setState(InfoPlayer info) {
        setCoord((double)info.getMyX(), (double)info.getMyY());
        if (!info.isAttachedBall()) {
            untieBound();
        }
    }
}
