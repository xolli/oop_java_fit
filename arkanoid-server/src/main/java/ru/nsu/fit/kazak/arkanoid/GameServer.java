package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NetworkException;
import ru.nsu.fit.kazak.arkanoid.exceptions.NoLevelFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class GameServer {
    private final int WIDTH_SCREEN = 600;
    private final int HEIGHT_SCREEN = 800;
    private final HashMap<Integer, Block> blocks;
    private final Pair<Receiver> receivers;
    private final Pair<Sender> senders;
    private Pair<Socket> sockets;
    private final Pair<Player> players;
    private Pair<ObjectInputStream> inputs;
    private Pair<ObjectOutputStream> outputs;
    private final AtomicBoolean gameProcess;

    public GameServer(int serverPort, int level) throws NetworkException {
        gameProcess = new AtomicBoolean(true);
        blocks = new HashMap<>();
        players = new Pair<>(new Player(WIDTH_SCREEN, HEIGHT_SCREEN, new Coord(WIDTH_SCREEN / 2 - 35, HEIGHT_SCREEN * 9 / 10), 1), new Player(WIDTH_SCREEN, HEIGHT_SCREEN, new Coord(WIDTH_SCREEN / 2 - 35, HEIGHT_SCREEN / 10), 2));
        generateMap(level);
        connect(serverPort);
        senders = new Pair<>(new Sender(outputs.getFirst()), new Sender(outputs.getSecond()));
        receivers = new Pair<>(new Receiver(players.getFirst(), inputs.getFirst(), false, gameProcess), new Receiver(players.getSecond(), inputs.getSecond(), true, gameProcess));
        senders.getFirst().send(blocks, 1);
        senders.getSecond().send(blocks, 2);
    }

    private void connect(int serverPort) throws NetworkException {
        Pair<ClientInit> initializers = new Pair<>();
        sockets = new Pair<>();
        try {
            ServerSocket serverSocket = new ServerSocket(serverPort, 2);
            sockets.setFirst(serverSocket.accept());
            System.out.println("Connect first client");
            initializers.setFirst(new ClientInit(sockets.getFirst()));
            initializers.getFirst().start();
            sockets.setSecond(serverSocket.accept());
            System.out.println("Connect second client");
            initializers.setSecond(new ClientInit(sockets.getSecond()));
            initializers.getSecond().start();
        } catch (IOException e) {
            throw new NetworkException();
        }
        inputs = new Pair<>();
        outputs = new Pair<>();
        try {
            inputs.setFirst(initializers.getFirst().getIn());
            inputs.setSecond(initializers.getSecond().getIn());
            outputs.setFirst(initializers.getFirst().getOut());
            outputs.setSecond(initializers.getSecond().getOut());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void start() throws NetworkException {
        receivers.getFirst().start();
        receivers.getSecond().start();
        Pair<Ball> balls = new Pair<>(new Ball(WIDTH_SCREEN / 2, HEIGHT_SCREEN * 9 / 10 - 10, 10), new Ball(WIDTH_SCREEN / 2, HEIGHT_SCREEN / 10 - 20, 10));
        players.getFirst().attachBall(balls.getFirst());
        players.getSecond().attachBall(balls.getSecond());
        Game game = new Game(balls, players, blocks, senders, WIDTH_SCREEN, HEIGHT_SCREEN, gameProcess);
        game.start();
        receivers.getFirst().interrupt();
        receivers.getSecond().interrupt();
        try {
            inputs.getFirst().close();
            inputs.getSecond().close();
            outputs.getFirst().close();
            outputs.getSecond().close();
            sockets.getFirst().close();
            sockets.getSecond().close();
        } catch (IOException e) {
            throw new NetworkException();
        }
        try {
            receivers.getFirst().join();
            receivers.getSecond().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void generateMap(int level) {
        Properties blocksTable = new Properties();
        String levelFileName = "level" + level + ".lvl";
        InputStream fin = getClass().getResourceAsStream(levelFileName);
        try {
            blocksTable.load(fin);
        } catch (Exception e) {
            throw new NoLevelFile(levelFileName);
        }
        Set keys = blocksTable.keySet();
        int i = 0;
        for (Object key : keys) {
            String[] coord = key.toString().split(",", 2);
            int x = Integer.parseInt(coord[0]);
            int y = Integer.parseInt(coord[1]);
            Block newBlock = new Block(new Coord(x, y));
            blocks.put(i, newBlock);
            ++i;
        }
    }
}
