package ru.nsu.fit.kazak.arkanoid;

import javax.swing.*;
import java.awt.Color;
import java.awt.geom.Area;
import java.awt.*;
import java.io.Serializable;

public class Sprite extends JComponent implements Serializable {
    protected final Coord pos = new Coord(0, 0);
    protected Shape figure;

    // https://stackoverflow.com/questions/15690846/java-collision-detection-between-two-shape-objects
    boolean cross(Sprite otherSprite) {
        Area areaA = new Area(figure);
        areaA.intersect(new Area(otherSprite.figure));
        return !areaA.isEmpty();
    }

    public Coord getPos() {
        return pos;
    }
}

