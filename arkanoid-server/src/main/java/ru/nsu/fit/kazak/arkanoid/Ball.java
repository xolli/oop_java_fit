package ru.nsu.fit.kazak.arkanoid;

import java.awt.geom.Ellipse2D;
import java.io.Serializable;

public class Ball extends Sprite implements Serializable {
    private int diameter;
    private Double direction;
    private Double speed;


    public Ball(int x, int y, int diameter) {
        direction = Math.PI / 2;
        speed = 0.;
        this.diameter = diameter;
        figure = new Ellipse2D.Double(x, y, diameter, diameter);
    }

    public Coord getPos() {
        return new Coord((int)((Ellipse2D.Double)figure).x, (int) ((Ellipse2D.Double)figure).y);
    }

    public void setCoord(Double x, Double y) {
        synchronized (figure) {
            ((Ellipse2D.Double)figure).x = x;
            ((Ellipse2D.Double)figure).y = y;
        }
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public void move() {
        Double dX = Math.cos(direction) * speed;
        Double dY = -Math.sin(direction) * speed;
        ((Ellipse2D.Double)figure).x += dX;
        ((Ellipse2D.Double)figure).y += dY;
    }

    private Double getRandomDouble(Double min, Double max) {
        return (Math.random()*((max - min))) + min;
    }

    public void push (DirectionSurface directionSurface) {
        Double delta = Math.PI / 15;
        if (directionSurface == DirectionSurface.HORIZONT && direction >= 0 && direction <= Math.PI / 2) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.HORIZONT && direction > Math.PI / 2 && direction <= Math.PI) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.HORIZONT && direction > Math.PI && direction <= Math.PI * 3 / 2) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.HORIZONT && direction > Math.PI * 3 / 2 && direction <= Math.PI * 2) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.VERTICAL && direction >= Math.PI / 2 && direction <= Math.PI) {
            direction = Math.PI - direction;
        }  else if (directionSurface == DirectionSurface.VERTICAL && direction >= Math.PI && direction <= Math.PI * 3 / 2) {
            direction = 3 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.VERTICAL && direction <= Math.PI / 2) {
            direction = Math.PI - direction;
        } else if (directionSurface == DirectionSurface.VERTICAL && direction >= Math.PI * 3 / 2) {
            direction = Math.PI * 3 / 2 - Math.abs(Math.PI * 3 / 2 - direction);
        }
        direction += getRandomDouble(-delta, delta);
        while (direction > Math.PI * 2) {
            direction -= Math.PI * 2;
        }
        if (direction < 0) {
            direction += Math.PI * 2;
        }
    }


    public boolean moveUp() {
        return direction < Math.PI;
    }

    public boolean moveDown() {
        return !moveUp();
    }

    public boolean moveLeft() {
        return direction > Math.PI / 2 && direction < Math.PI * 3 / 2;
    }

    public boolean moveRight() {
        return !moveLeft();
    }
}
