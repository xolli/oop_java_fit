package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NetworkException;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class Sender {
    private final int WIDTH_SCREEN = 600;
    private final int HEIGHT_SCREEN = 800;
    ObjectOutputStream out;

    Sender (ObjectOutputStream out) throws NetworkException {
        this.out = out;
    }

    public void send(HashMap<Integer, Block> blocks, int numberPlayer) throws NetworkException {
        try {
            out.writeObject(constructMetaBlocks(blocks, numberPlayer));
            out.flush();
        } catch (IOException e) {
            System.out.println("Error 1");
            e.printStackTrace();
            throw new NetworkException();
        }
    }

    private HashMap<Integer, MetaInfoBlock> constructMetaBlocks(HashMap<Integer, Block> blocks, int numberPlayer) {
        HashMap<Integer, MetaInfoBlock> ret = new HashMap<>();
        for (Map.Entry<Integer, Block> entry : blocks.entrySet()) {
            MetaInfoBlock meta = entry.getValue().getMetaInfo();
            if (numberPlayer == 2) {
                meta.inversion(WIDTH_SCREEN, HEIGHT_SCREEN);
            }
            ret.put(entry.getKey(), meta);
        }
        return ret;
    }

    public void send(InfoGame info, int numberPlayer) throws NetworkException {
        try {
            out.writeObject(info);
            out.flush();
        } catch (IOException e) {
            throw new NetworkException();
        }
    }
}
