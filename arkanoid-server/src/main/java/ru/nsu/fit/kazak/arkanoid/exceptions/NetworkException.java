package ru.nsu.fit.kazak.arkanoid.exceptions;

public class NetworkException extends Exception {
    public NetworkException() {
        super("Network Error");
    }
}
