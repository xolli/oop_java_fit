package ru.nsu.fit.kazak.arkanoid.exceptions;

public class NoLevelFile extends RuntimeException {
    public NoLevelFile(String levelFileName) {
        super("No level file \"" + levelFileName + "\"");
    }
}
