package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NetworkException;

public class Main {

    public static void main(String[] args) {
        GameServer gameServer;
        try {
            gameServer = new GameServer(6666, 1);
            gameServer.start();
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
        }
    }

}
