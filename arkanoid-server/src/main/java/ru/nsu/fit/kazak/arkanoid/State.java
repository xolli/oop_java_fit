package ru.nsu.fit.kazak.arkanoid;

public enum State {
    GAME,
    GAMEOVER,
    WIN
}
