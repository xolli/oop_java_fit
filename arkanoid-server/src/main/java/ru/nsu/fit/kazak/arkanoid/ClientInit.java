package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NetworkException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ClientInit extends Thread {
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private final Socket socket;
    private final Lock lock = new ReentrantLock();
    private final Condition initIn = lock.newCondition();
    private final Condition initOut = lock.newCondition();

    public ClientInit(Socket socket) throws NetworkException {
        this.socket = socket;
        in = null;
        out = null;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            out = new ObjectOutputStream(socket.getOutputStream());
            initOut.signalAll();
            in = new ObjectInputStream(socket.getInputStream());
            initIn.signalAll();
            lock.unlock();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public ObjectInputStream getIn() throws InterruptedException {
        lock.lock();
        if (in == null) {
            initIn.await();
        }
        lock.unlock();
        return in;
    }

    public ObjectOutputStream getOut() throws InterruptedException {
        lock.lock();
        if (out == null) {
            initOut.await();
        }
        lock.unlock();
        return out;
    }
}
