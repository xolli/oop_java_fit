package ru.nsu.fit.kazak.arkanoid;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.concurrent.atomic.AtomicBoolean;

public class Receiver extends Thread {
    private final ObjectInputStream in;
    boolean invert;
    Player player;
    private final AtomicBoolean gameProcess;

    public Receiver(Player player, ObjectInputStream in, boolean invert, AtomicBoolean gameProcess) {
        this.player = player;
        this.in = in;
        this.invert = invert;
        this.gameProcess = gameProcess;
    }

    @Override
    public void run() {
        while(true) {
            try {
                if (!gameProcess.get()) {
                    return;
                }
                InfoPlayer info = (InfoPlayer) in.readObject();
                if (!info.isConnected()) {
                    return;
                }
                if (invert) {
                    info.invert();
                }
                player.setState(info);
            } catch (IOException | ClassNotFoundException e) {
                return;
            }
        }
    }
}
