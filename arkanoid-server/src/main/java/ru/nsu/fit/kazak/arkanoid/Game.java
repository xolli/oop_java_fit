package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NetworkException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class Game {
    private Pair<Player> players;
    private Pair<Ball> balls;
    private HashMap<Integer, Block> blocks;
    private Pair<Sender> senders;
    private Size sizeWindow;
    private AtomicBoolean processGame;

    public Game(Pair<Ball> balls, Pair<Player> players, HashMap<Integer, Block> blocks, Pair<Sender> senders, int width, int height, AtomicBoolean processGame) {
        this.players = players;
        this.balls = balls;
        this.blocks = blocks;
        this.senders = senders;
        this.processGame = processGame;
        sizeWindow = new Size(width, height);
    }

    public void start() throws NetworkException {
        while (true) {
            State state = nextFrame();
            if (state == State.WIN) {
                send(-2);
                processGame.set(false);
                return;
            } else if (state == State.GAMEOVER) {
                send(-3);
                processGame.set(false);
                return;
            }
            try {
                Thread.sleep(2);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
                return;
            }
        }
    }

    public State nextFrame() throws NetworkException {
        State state;
        if ((state = moveBall(balls.getFirst())) != State.GAME) {
            return state;
        }
        if ((state = moveBall(balls.getSecond())) != State.GAME) {
            return state;
        }
        if (blocks.isEmpty()) {
            return State.WIN;
        }
        return State.GAME;
    }

    private State moveBall(Ball ball) throws NetworkException {
        ball.move();
        send(-1);
        if ((ball.getPos().getY() < 0 && ball.moveUp())) {
            return State.GAMEOVER;
        }
        if (ball.getPos().getY() + ball.getDiameter() > sizeWindow.getHeight() && ball.moveDown()) {
            return State.GAMEOVER;
        }
        if ((ball.getPos().getX() < 0 && ball.moveLeft()) || (ball.getPos().getX() + ball.getDiameter() > sizeWindow.getWidth() && ball.moveRight())) {
            ball.push(DirectionSurface.VERTICAL);
        }
        if (players.getFirst().cross(ball) && ball.moveDown()) {
            ball.push(DirectionSurface.HORIZONT);
        }
        if (players.getSecond().cross(ball) && ball.moveUp()) {
            ball.push(DirectionSurface.HORIZONT);
        }
        Iterator<Map.Entry<Integer, Block>> entryIterator = blocks.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<Integer, Block> entry = entryIterator.next();
            Block block = entry.getValue();
            if (block.cross(ball)) {
                Coord boundPos = ball.getPos();
                Coord blockPos = block.getPos();
                Size blockSize = block.getSizeBlock();
                if (boundPos.getY() - 2 <= blockPos.getY() + blockSize.getHeight() || boundPos.getY() + ball.getDiameter() + 2 <= blockPos.getY()) {
                    ball.push(DirectionSurface.HORIZONT);
                } else {
                    ball.push(DirectionSurface.VERTICAL);
                }
                entryIterator.remove();
                send(entry.getKey());
                break;
            }
        }
        return State.GAME;
    }

    private void send(int numberDeadBlock) throws NetworkException {
        if (!processGame.get()) {
            return;
        }
        senders.getFirst().send(new InfoGame(balls.getFirst().getPos(), balls.getSecond().getPos(), players.getSecond().getPos(), numberDeadBlock, sizeWindow.getWidth(), 1), 1);
        senders.getSecond().send(new InfoGame(balls.getFirst().getPos(), balls.getSecond().getPos(), players.getFirst().getPos(), numberDeadBlock, sizeWindow.getWidth(), 2), 2);
    }
}
