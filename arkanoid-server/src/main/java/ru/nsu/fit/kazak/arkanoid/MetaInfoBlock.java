package ru.nsu.fit.kazak.arkanoid;

import java.io.Serializable;

public class MetaInfoBlock implements Serializable {
    private static final long serialVersionUID = 4L;
    private final Coord pos;

    public MetaInfoBlock(Coord pos) {
        this.pos = pos;
    }

    public MetaInfoBlock(Block block) {
        this(block.getPos());
    }

    public Coord getPos() {
        return pos;
    }

    public Block constructBlock() {
        return new Block(pos);
    }

    public void inversion(int windowWidth, int windowHeight) {
        pos.setX(windowWidth - pos.getX() - 50);
        pos.setY(windowHeight - pos.getY());
    }
}
