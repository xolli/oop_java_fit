package ru.nsu.fit.kazak.arkanoid;

import java.awt.geom.Rectangle2D;

public class Block extends Sprite {
    private static final long serialVersionUID = 4L;
    private Size size;

    private Integer getRandomInt(Integer min, Integer max) {
        return (int) (min + Math.random() * (max - min + 1));
    }

    public Block(Coord setPos) {
        size = new Size(50, 20);
        this.pos.setX(setPos.getX());
        this.pos.setY(setPos.getY());
        figure = new Rectangle2D.Double(setPos.getX(), setPos.getY(), size.getWidth(), size.getHeight());
    }

    public Size getSizeBlock() {
        return size;
    }

    public MetaInfoBlock getMetaInfo() {
        return new MetaInfoBlock(pos);
    }
}
