import ru.nsu.fit.kazak.arkanoid.*;
import ru.nsu.fit.kazak.arkanoid.exceptions.NoSettingsFile;
import ru.nsu.fit.kazak.arkanoid.network.exceptions.NetworkException;

public final class Main {
    public static void main(String[] args) {
        Game game;
        try {
            game = new Game(6666);
        } catch (NoSettingsFile | NetworkException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
            return;
        }
        game.start();
    }
}
