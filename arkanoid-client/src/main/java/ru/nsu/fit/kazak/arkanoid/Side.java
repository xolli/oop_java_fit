package ru.nsu.fit.kazak.arkanoid;

import java.io.Serializable;

public enum Side implements Serializable {
    LEFT, RIGHT
}
