package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.network.NetworkSender;
import ru.nsu.fit.kazak.arkanoid.network.exceptions.NetworkException;

import javax.swing.*;
import java.awt.event.*;
import java.util.concurrent.atomic.AtomicBoolean;


public class GameController extends JComponent implements MouseMotionListener, MouseListener {
    private static final int IFW = JComponent.WHEN_IN_FOCUSED_WINDOW;
    NetworkSender sender;
    Player myPlayer;
    private final AtomicBoolean gameProcess;

    public GameController (NetworkSender sender, Player myPlayer, AtomicBoolean gameProcess) {
        this.sender = sender;
        this.myPlayer = myPlayer;
        this.gameProcess = gameProcess;
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        mousePressed(mouseEvent);
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {}

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {}

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        myPlayer.setCoord((double)mouseEvent.getX() - 35, (double)mouseEvent.getY());
        if (!gameProcess.get()) {
            return;
        }
        try {
            sender.send(myPlayer);
        } catch (NetworkException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {}

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {}

    @Override
    public void mouseExited(MouseEvent mouseEvent) {}

    public void inputMapPut(String keyString, String action) {
        getInputMap(IFW).put(KeyStroke.getKeyStroke(keyString), action);
    }

    public void actionMapPut(String actionString, AbstractAction action) {
        getActionMap().put(actionString, action);
    }

}
