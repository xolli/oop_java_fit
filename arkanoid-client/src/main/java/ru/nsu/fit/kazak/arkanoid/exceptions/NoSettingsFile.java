package ru.nsu.fit.kazak.arkanoid.exceptions;

public class NoSettingsFile extends Exception  {
    public NoSettingsFile(String settingsFileName) {
        super("No setings file with name: " + settingsFileName);
    }
}
