package ru.nsu.fit.kazak.arkanoid;

import java.io.Serializable;

public class Pair<T>  implements Serializable {
    private T first;
    private T second;

    public Pair (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }
}
