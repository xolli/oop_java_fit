package ru.nsu.fit.kazak.arkanoid.network.exceptions;

public class NetworkException extends Exception {
    public NetworkException() {
        super("Network connected error");
    }
}
