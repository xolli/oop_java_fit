package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NoSettingsFile;
import ru.nsu.fit.kazak.arkanoid.network.NetworkReceiver;
import ru.nsu.fit.kazak.arkanoid.network.NetworkSender;
import ru.nsu.fit.kazak.arkanoid.network.exceptions.NetworkException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class Game extends Thread {
    private final int WIDTH_SCREEN = 600;
    private final int HEIGHT_SCREEN = 800;
    private final Pair <Ball> balls;
    private final Pair<Player> players;
    private final Map<Integer, Block> blocks;
    private static final String MOVE_LEFT = "MOVE_LEFT";
    private static final String MOVE_RIGHT = "MOVE_RIGHT";
    private static final String UNTIE = "UNTIE";
    private final GameController controller;
    private final NetworkSender sender;
    private final NetworkReceiver receiver;
    JFrame window;
    private final Socket socket;
    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    private final AtomicBoolean gameProcess;

    public Game(int port) throws NoSettingsFile, NetworkException, ClassNotFoundException {
        this.gameProcess = new AtomicBoolean(true);
        window = new JFrame("Arkanoid online");
        window.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
        window.setSize(new Dimension(WIDTH_SCREEN, HEIGHT_SCREEN));
        window.setResizable(false);
        players = new Pair<>(new Player(window.getWidth(), window.getHeight(), new Coord(WIDTH_SCREEN / 2 - 35, HEIGHT_SCREEN * 9 / 10)), new Player(window.getWidth(), window.getHeight(), new Coord(WIDTH_SCREEN / 2 - 35, HEIGHT_SCREEN / 10)));
        balls = new Pair<>(new Ball(0, HEIGHT_SCREEN * 9 / 10 - 10, 10, Color.PINK), new Ball(WIDTH_SCREEN / 2, HEIGHT_SCREEN / 10 - 20, 10, Color.BLUE));
        try {
            System.out.println("Wait connect");
            socket = new Socket(InetAddress.getLocalHost(), port);
            System.out.println("Connect!");
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            throw new NetworkException();
        }
        receiver = new NetworkReceiver(in);
        sender = new NetworkSender(out);
        HashMap<Integer, Block> _blocks = receiver.receiveBlocks();
        //blocks = Collections.synchronizedMap(_blocks);
        blocks = new ConcurrentHashMap<>(_blocks);
        receiver.setGameChanger(new GameChanger(players.getSecond(), balls.getFirst(), balls.getSecond(), blocks, gameProcess));
        controller = new GameController(sender, players.getFirst(), gameProcess);
        loadOptions();
        window.add(controller);
    }

    private void loadOptions() throws NoSettingsFile {
        Properties keysTable = new Properties();
        String settingsFile = "options.txt";
        InputStream fin = getClass().getResourceAsStream(settingsFile);
        try {
            keysTable.load(fin);
        } catch (Exception e) {
            throw new NoSettingsFile(settingsFile);
        }
        Set actions = keysTable.keySet();
        for (Object action : actions) {
            controller.inputMapPut(keysTable.get(action).toString(), action.toString());
        }
        controller.actionMapPut(MOVE_RIGHT, new MoveAction(Side.RIGHT));
        controller.actionMapPut(MOVE_LEFT, new MoveAction(Side.LEFT));
        controller.actionMapPut(UNTIE, new UntieAction());
    }

    @Override
    public void run() {
        receiver.start();
        window.addMouseMotionListener(controller);
        window.addMouseListener(controller);
        players.getFirst().attachBall();
        GameViewer gameViewer;
        gameViewer = new GameViewer(balls, players, blocks, window.getWidth(), window.getHeight(), gameProcess);
        window.add(gameViewer);
        window.setVisible(true);
        boolean state = true;
        while (state) {
            state = gameViewer.nextFrame();
            try {
                Thread.sleep(2);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private class MoveAction extends AbstractAction {
        Side side;

        MoveAction(Side side) {
            this.side = side;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            players.getFirst().move(side);
            if (!gameProcess.get()) {
                return;
            }
            try {
                sender.send(players.getFirst());
            } catch (NetworkException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    private class UntieAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            players.getFirst().untieBall();
            if (!gameProcess.get()) {
                return;
            }
            try {
                sender.send(players.getFirst());
            } catch (NetworkException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
