package ru.nsu.fit.kazak.arkanoid;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.*;

public class Block extends Sprite implements Externalizable {
    private static final long serialVersionUID = 4L;
    private Size size;

    private Integer getRandomInt(Integer min, Integer max) {
        return (int) (min + Math.random() * (max - min + 1));
    }

    public Block(Coord pos) {
        size = new Size(50, 20);
        this.pos = pos;
        figure = new Rectangle2D.Double(pos.getX(), pos.getY(), size.getWidth(), size.getHeight());
        Color[] colors = {Color.RED, Color.GREEN, Color.BLUE, Color.PINK, Color.CYAN, Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.YELLOW};
        color = colors[getRandomInt(0, colors.length - 1)];
    }

    public Size getSizeBlock() {
        return size;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(size);
        out.writeObject(pos);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        size = (Size) in.readObject();
        pos = (Coord) in.readObject();
        figure = new Rectangle2D.Double(pos.getX(), pos.getY(), size.getWidth(), size.getHeight());
        Color[] colors = {Color.RED, Color.GREEN, Color.BLUE, Color.PINK, Color.CYAN, Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.YELLOW};
        color = colors[getRandomInt(0, colors.length - 1)];
    }
}
