package ru.nsu.fit.kazak.arkanoid;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class GameChanger {
    Player otherPlayer;
    Ball ball_1;
    Ball ball_2;
    Map<Integer, Block> blocks;
    private final AtomicBoolean gameProcess;

    public GameChanger(Player otherPlayer, Ball ball_1, Ball ball_2, Map<Integer, Block> blocks, AtomicBoolean gameProcess) {
        this.otherPlayer = otherPlayer;
        this.ball_1 = ball_1;
        this.ball_2 = ball_2;
        this.blocks = blocks;
        this.gameProcess = gameProcess;
    }

    public State changeState(InfoGame info) {
        otherPlayer.setCoord((double) info.getCoordXOtherPlayer(), (double)otherPlayer.getPos().getY());
        ball_1.setCoord((double)info.getCoordBall_1().getX(), (double)info.getCoordBall_1().getY());
        ball_2.setCoord((double)info.getCoordBall_2().getX(), (double)info.getCoordBall_2().getY());
        if (info.getNumberDeadBlock() >= 0) {
            blocks.remove(info.getNumberDeadBlock());
        } else if (info.getNumberDeadBlock() == -3) {
            gameover();
            gameProcess.set(false);
            return State.GAMEOVER;
        } else if (info.getNumberDeadBlock() == -2) {
            win();
            gameProcess.set(false);
            return State.WIN;
        }
        return State.GAME;
    }

    private void gameover() {
        GameOverWindow gameOver = new GameOverWindow();
        gameOver.showWindow();
    }

    private void win() {
        WinWindow win = new WinWindow();
        win.showWindow();
    }
}
