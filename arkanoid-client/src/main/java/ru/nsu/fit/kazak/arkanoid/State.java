package ru.nsu.fit.kazak.arkanoid;

import java.io.Serializable;

public enum State  implements Serializable {
    GAME,
    GAMEOVER,
    WIN
}
