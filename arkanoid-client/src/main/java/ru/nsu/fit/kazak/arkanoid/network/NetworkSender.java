package ru.nsu.fit.kazak.arkanoid.network;

import ru.nsu.fit.kazak.arkanoid.*;
import ru.nsu.fit.kazak.arkanoid.network.exceptions.NetworkException;

import java.io.*;

public class NetworkSender {
    private final ObjectOutputStream out;

    public NetworkSender(ObjectOutputStream out) {
        this.out = out;
    }

    private void sendDataPlayer(Player player, boolean isConnected) throws NetworkException {
        try {
            out.writeObject(new InfoPlayer(player, isConnected));
        } catch (IOException e) {
            throw new NetworkException();
        }
    }

    public void send(Player player) throws NetworkException {
        sendDataPlayer(player, true);
    }

    public void send(Player player, boolean isConnected) throws NetworkException {
        sendDataPlayer(player, isConnected);
    }
}
