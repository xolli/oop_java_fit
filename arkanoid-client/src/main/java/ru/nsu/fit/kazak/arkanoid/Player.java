package ru.nsu.fit.kazak.arkanoid;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

public class Player extends Sprite {
    private final int widthPlayer, heightPlayer;
    private final int widthScreen;
    private boolean isAttachedBall;

    public Player(int widthScreen, int heightScreen, Coord coord) {
        this.widthScreen = widthScreen;
        isAttachedBall = false;
        widthPlayer = 70;
        heightPlayer = 20;
        pos = coord;
        figure = new Rectangle2D.Double(coord.getX(), coord.getY(), widthPlayer, heightPlayer);
        color = Color.GREEN;
    }

    public void attachBall() {
        isAttachedBall = true;
    }

    public void untieBall() {
        isAttachedBall = false;
    }

    public void setCoord(Double x, Double y) {
        x += widthPlayer / 2;
        if ((isAttachedBall && (x + 5 < widthScreen) && (x - 5) > 0) ||
            (!isAttachedBall && x < widthScreen && x > 0)) {
            synchronized (figure) {
                ((Rectangle.Double)figure).x = x - (double)widthPlayer / 2;
            }
            pos.setX((int)((Rectangle.Double)figure).x);
            pos.setY((int)((Rectangle.Double)figure).y);
        }
    }

    public boolean isAttachedBall() {
        return isAttachedBall;
    }

    public void move(Side side) {
        if (side == Side.LEFT) {
            goLeft();
        } else if (side == Side.RIGHT) {
            goRight();
        }
    }

    public void goRight() {
        double offset = 10.;
        if (((Rectangle.Double)figure).x + widthPlayer + offset <= widthScreen) {
            ((Rectangle.Double)figure).x += offset;
        }
        pos.setX((int)((Rectangle.Double)figure).x);
        pos.setY((int)((Rectangle.Double)figure).y);
    }

    public void goLeft() {
        double offset = 10.;
        if (((Rectangle.Double)figure).x  - offset >= 0) {
            ((Rectangle.Double)figure).x -= offset;
        }
        pos.setX((int)((Rectangle.Double)figure).x);
        pos.setY((int)((Rectangle.Double)figure).y);
    }
}
