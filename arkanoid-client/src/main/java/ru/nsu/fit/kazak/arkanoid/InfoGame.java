package ru.nsu.fit.kazak.arkanoid;

import java.io.Serializable;

public class InfoGame implements Serializable {
    private static final long serialVersionUID = 1L;
    private final int coordBall1X;
    private final int coordBall1Y;
    private final int coordBall2X;
    private final int coordBall2Y;
    private final int numberDeadBlock;
    private final int coordXOtherPlayer;

    public InfoGame(Coord coordBall_1, Coord coordBall_2, Coord coordOtherPlayer, int numberDeadBlock, int widthScreen) {
        this.numberDeadBlock = numberDeadBlock;
        coordBall1X = coordBall_1.getX();
        coordBall1Y = coordBall_1.getY();
        coordBall2X = coordBall_2.getX();
        coordBall2Y = coordBall_2.getY();
        coordXOtherPlayer = widthScreen - coordOtherPlayer.getX() - 70;
    }

    public Coord getCoordBall_1() {
        return new Coord(coordBall1X, coordBall1Y);
    }

    public Coord getCoordBall_2() {
        return new Coord(coordBall2X, coordBall2Y);
    }

    public int getNumberDeadBlock() {
        return numberDeadBlock;
    }

    public int getCoordXOtherPlayer() {
        return coordXOtherPlayer;
    }
}
