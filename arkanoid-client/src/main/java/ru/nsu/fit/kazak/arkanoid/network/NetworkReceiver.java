package ru.nsu.fit.kazak.arkanoid.network;

import ru.nsu.fit.kazak.arkanoid.*;
import ru.nsu.fit.kazak.arkanoid.network.exceptions.NetworkException;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class NetworkReceiver extends Thread {
    private final ObjectInputStream in;
    private GameChanger changer;

    public NetworkReceiver(ObjectInputStream in) {
        this.in = in;
    }

    public void setGameChanger(GameChanger changer) {
        this.changer = changer;
    }

    public HashMap<Integer, Block> receiveBlocks() throws NetworkException, ClassNotFoundException {
        try {
            HashMap<Integer, MetaInfoBlock> metaBlocks = (HashMap<Integer, MetaInfoBlock>) in.readObject();
            return constructHashMapBlock(metaBlocks);
        } catch (IOException e) {
            e.printStackTrace();
            throw new NetworkException();
        }
    }

    private HashMap<Integer, Block> constructHashMapBlock(HashMap<Integer, MetaInfoBlock> metaBlocks) {
        HashMap<Integer, Block> ret = new HashMap<>();
        for (Map.Entry<Integer, MetaInfoBlock> entry : metaBlocks.entrySet()) {
            ret.put(entry.getKey(), entry.getValue().constructBlock());
        }
        return ret;
    }

    @Override
    public void run() {
        try {
            while (true) {
                InfoGame info = (InfoGame)in.readObject();
                if (changer.changeState(info) != ru.nsu.fit.kazak.arkanoid.State.GAME) {
                    return;
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
