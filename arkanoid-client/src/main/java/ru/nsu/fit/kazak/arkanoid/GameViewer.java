package ru.nsu.fit.kazak.arkanoid;

import javax.swing.*;
import java.awt.*;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class GameViewer extends JPanel {
    private final int WIDTH_SCREEN = 600;
    private final int HEIGHT_SCREEN = 800;
    private Map<Integer, Block> blocks;
    private Pair <Ball> balls;
    private Size size;
    private Pair <Player> players;
    private Integer level;
    private final AtomicBoolean gameProcess;

    public GameViewer(Pair <Ball> balls, Pair<Player> players, Map<Integer, Block> blocks, int width, int height, AtomicBoolean gameProcess) {
        this.balls = balls;
        this.players = players;
        this.blocks = blocks;
        size = new Size(width, height);
        this.gameProcess = gameProcess;
    }

    public boolean nextFrame() {
        repaint();
        return gameProcess.get();
    }

    @Override
    // https://stackoverflow.com/questions/33784268/fill-a-shape-with-a-random-color-in-java
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        players.getFirst().paint(g2);
        players.getSecond().paint(g2);
        balls.getFirst().paint(g2);
        balls.getSecond().paint(g2);
        for (int key : blocks.keySet()) {
            blocks.get(key).paint(g2);
        }
    }
}
