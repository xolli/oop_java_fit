package ru.nsu.fit.kazak.arkanoid;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.io.Serializable;

public class Ball extends Sprite implements Serializable {
    private int diameter;

    public Ball(int x, int y, int diameter, Color color) {
        this.diameter = diameter;
        this.color = color;
        figure = new Ellipse2D.Double(x, y, diameter, diameter);
    }

    public Coord getPos() {
        return new Coord((int)((Ellipse2D.Double)figure).x, (int) ((Ellipse2D.Double)figure).y);
    }

    public void setCoord(Double x, Double y) {
        synchronized (figure) {
            ((Ellipse2D.Double)figure).x = x;
            ((Ellipse2D.Double)figure).y = y;
        }
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }
}
