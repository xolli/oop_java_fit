package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.kazak.calc.exception.EmptyStackException;
import ru.nsu.kazak.calc.exception.NumArgsException;
import ru.nsu.kazak.calc.exception.ZeroDivizionException;

import static org.junit.Assert.*;

public class DivisionTest {
    @Test
    public void process() {
        Context context = new Context();
        Arguments argDivision = new Arguments(new String[] {});
        Division divisionCommand = new Division();
        context.addNum(2.);
        context.addNum(10.);
        divisionCommand.process(argDivision, context);
        Assert.assertEquals(10. / 2., context.getNum(), 0.);
    }

    @Test (expected = NumArgsException.class)
    public void wrongNumArgs() {
        Context context = new Context();
        Arguments argDivision = new Arguments(new String[] {"arg"});
        Division divisionCommand = new Division();
        divisionCommand.process(argDivision, context);
    }

    @Test (expected = ZeroDivizionException.class)
    public void zeroDivizionException() {
        Context context = new Context();
        Arguments argDivision = new Arguments(new String[] {});
        Division divisionCommand = new Division();
        context.addNum(0.);
        context.addNum(10.);
        divisionCommand.process(argDivision, context);
    }
}