package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.kazak.calc.exception.*;

public class SqrtTest {
    @Test
    public void process() {
        Context context = new Context();
        context.addNum(16.);
        Arguments args = new Arguments(new String[] {});
        Sqrt command = new Sqrt();
        command.process(args, context);
        Assert.assertEquals(4., context.getNum(), 0.);
    }

    @Test (expected = NumArgsException.class)
    public void wrongNumArgs() {
        Context context = new Context();
        context.addNum(16.);
        Arguments args = new Arguments(new String[] {"arg"});
            Sqrt command = new Sqrt();
            command.process(args, context);
    }

    @Test (expected = NegativeSqrtException.class)
    public void negative() {
        Context context = new Context();
        context.addNum(-16.);
        Arguments args = new Arguments(new String[] {});
        Sqrt command = new Sqrt();
        command.process(args, context);
    }
}