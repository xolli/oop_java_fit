package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.kazak.calc.exception.EmptyStackException;
import ru.nsu.kazak.calc.exception.NumArgsException;

public class PlusTest {
    @Test
    public void process() throws EmptyStackException, NumArgsException {
        Context context = new Context();
        Arguments argDivision = new Arguments(new String[] {});
        Plus command = new Plus();
        context.addNum(2.);
        context.addNum(10.);
        command.process(argDivision, context);
        Assert.assertEquals(10. + 2., context.getNum(), 0.);
    }

    @Test
    public void numArgsException() {
        Context context = new Context();
        Arguments argDivision = new Arguments(new String[] {""});
        try {
            Plus command = new Plus();
            command.process(argDivision, context);
            Assert.fail("Expected NumArgsException");
        } catch (NumArgsException e) {
            Assert.assertNotEquals("", e.getMessage());
        } catch (EmptyStackException e) {
            Assert.fail("Expected NumArgsException");
        }
    }
}