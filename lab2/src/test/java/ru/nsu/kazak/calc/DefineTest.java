package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.kazak.calc.exception.*;

public class DefineTest {
    @Test
    public void process() {
        Context context = new Context();
        Arguments argDefine = new Arguments(new String[] {"a", "1"});
        Define defineCommand = new Define();
        defineCommand.process(argDefine, context);
        context.addNum("a");
        Assert.assertEquals(1., context.getVar("a"), 0.);
    }

    @Test (expected = NumArgsException.class)
    public void wrongNumException() {
        Context context = new Context();
        Arguments argDefine = new Arguments(new String[] {"a"});
        Define defineCommand = new Define();
        defineCommand.process(argDefine, context);
    }

    @Test (expected = DefineArgumentsException.class)
    public void wrongArgs() {
        Context context = new Context();
        Arguments argDefine = new Arguments(new String[] {"a", "bcd"});
        Define defineCommand = new Define();
        defineCommand.process(argDefine, context);
    }

    @Test (expected = DefineArgumentsException.class)
    public void wrongArgs2() {
        Context context = new Context();
        Arguments argDefine = new Arguments(new String[] {"2", "85"});
        Define defineCommand = new Define();
        defineCommand.process(argDefine, context);
    }

    @Test (expected = DefineArgumentsException.class)
    public void wrongArgs3() {
        Context context = new Context();
        Arguments argDefine = new Arguments(new String[] {"123", "sdf"});
        Define defineCommand = new Define();
        defineCommand.process(argDefine, context);
    }
}