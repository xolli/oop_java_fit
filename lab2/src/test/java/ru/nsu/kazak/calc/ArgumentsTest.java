package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;

public class ArgumentsTest {
    @Test
    public void numArgs() {
        Arguments args = new Arguments(new String[]{"arg1", "arg2", "arg3"});
        Assert.assertEquals(3, args.numArgs());
    }

    @Test
    public void getArg() {
        Arguments args = new Arguments(new String[]{"arg0", "arg1", "arg2"});
        Assert.assertEquals("arg0", args.getArg(0));
        Assert.assertEquals("arg1", args.getArg(1));
        Assert.assertEquals("arg2", args.getArg(2));
    }
}
