package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.kazak.calc.exception.EmptyStackException;
import ru.nsu.kazak.calc.exception.NameException;
import ru.nsu.kazak.calc.exception.NumArgsException;
import ru.nsu.kazak.calc.exception.ZeroDivizionException;

import static org.junit.Assert.*;

public class PopTest {
    @Test
    public void process() {
        Context context = new Context();
        context.addNum(1.);
        context.addNum(2.);
        Arguments arguments = new Arguments(new String[] {});
        Pop command = new Pop();
        command.process(arguments, context);
        Assert.assertEquals(1., context.getNum(), 0.);
    }

    @Test (expected = EmptyStackException.class)
    public void emptyStack() {
        Context context = new Context();
        Arguments arguments = new Arguments(new String[] {});
        Pop command = new Pop();
        command.process(arguments, context);
    }

    @Test (expected = NumArgsException.class)
    public void wrongNumArg() {
        Context context = new Context();
        context.addNum(1.);
        context.addNum(2.);
        Arguments arguments = new Arguments(new String[] {""});
        Pop command = new Pop();
        command.process(arguments, context);
    }
}