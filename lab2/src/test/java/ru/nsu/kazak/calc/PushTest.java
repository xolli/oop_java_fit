package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.kazak.calc.exception.EmptyStackException;
import ru.nsu.kazak.calc.exception.NameException;
import ru.nsu.kazak.calc.exception.NumArgsException;

import static org.junit.Assert.*;

public class PushTest {
    @Test
    public void process() {
        Context context = new Context();
        Arguments args = new Arguments(new String[] {"5"});
        Push push = new Push();
        push.process(args, context);
        Assert.assertEquals(5., context.getNum(), 0.);
    }

    @Test (expected = NumArgsException.class)
    public void wrongNumArgs() {
        Context context = new Context();
        Arguments args = new Arguments(new String[] {});
        Push command = new Push();
        command.process(args, context);
    }

    @Test (expected = NameException.class)
    public void nameException() {
        Context context = new Context();
        Arguments args = new Arguments(new String[] {"nameVar"});
        Push push = new Push();
        push.process(args, context);
    }
}