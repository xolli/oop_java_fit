package ru.nsu.kazak.calc;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.kazak.calc.exception.EmptyStackException;
import ru.nsu.kazak.calc.exception.NumArgsException;

import static org.junit.Assert.*;

public class MinusTest {
    @Test
    public void process() {
        Context context = new Context();
        Arguments argDivision = new Arguments(new String[] {});
        Minus command = new Minus();
        context.addNum(2.);
        context.addNum(10.);
        command.process(argDivision, context);
        Assert.assertEquals(10. - 2., context.getNum(), 0.);
    }

    @Test (expected = NumArgsException.class)
    public void wrongNumArgs() {
        Context context = new Context();
        Arguments argDivision = new Arguments(new String[] {"0"});
        Minus command = new Minus();
        command.process(argDivision, context);
    }
}