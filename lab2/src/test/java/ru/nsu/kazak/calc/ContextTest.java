package ru.nsu.kazak.calc;

import ru.nsu.kazak.calc.exception.*;

import org.junit.Assert;
import org.junit.Test;

public class ContextTest {
    @Test
    public void getNum() {
        Context context = new Context();
        context.addNum(0.);
        Assert.assertEquals(0., context.getNum(), 0.);
        context.addNum(2.);
        Assert.assertEquals(2., context.getNum(), 0.);
        context.addNum(1024.);
        Assert.assertEquals(1024., context.getNum(), 0.);
    }

    @Test (expected = EmptyStackException.class)
    public void emptyStack() {
        Context context = new Context();
        context.getNum();
    }

    @Test
    public void addNum() {
        Context context = new Context();
        context.addNum(5.);
        Assert.assertEquals(5., context.getNum(), 0.);
        context.addNum(456.);
        Assert.assertEquals(456., context.getNum(), 0.);
        context.addNum(2048.);
        Assert.assertEquals(2048., context.getNum(), 0.);
    }

    @Test
    public void delNum() {
        Context context = new Context();
        context.addNum(1.);
        context.addNum(2.);
        context.delNum();
        Assert.assertEquals(1., context.getNum(), 0.);
    }

    @Test
    public void addVar() {
        Context context = new Context();
        context.addVar("a", 0.);
        context.addNum("a");
        Assert.assertEquals(0., context.getNum(), 0.);
        context.addVar("varName", 5.);
        context.addNum("varName");
        Assert.assertEquals(5., context.getNum(), 0.);
    }

    @Test(expected = NameException.class)
    public void noExistVarName() {
        Context context = new Context();
        context.addNum("varName");
    }
}