package ru.nsu.kazak.calc;

public abstract class BinaryCommand extends Command {
    protected int minSizeStack() {
        return 2;
    }
}
