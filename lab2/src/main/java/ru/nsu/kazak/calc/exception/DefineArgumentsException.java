package ru.nsu.kazak.calc.exception;

public class DefineArgumentsException extends NoCriticalException {
    public DefineArgumentsException() {
        super("Invalid arguments in DEFINE");
    }
}
