package ru.nsu.kazak.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.kazak.calc.exception.*;

public class Sqrt extends Command {
    private static final Logger logger = LogManager.getLogger();

    public void process(Arguments argv, Context context) {
        logger.debug("Operation: SQRT");
        if (argv.numArgs() != 0) {
            throw new NumArgsException(0, argv.numArgs());
        }
        Double a = context.getNum();
        context.delNum();
        if (a < 0) {
            context.addNum(a);
            throw new NegativeSqrtException();
        }
        context.addNum(Math.sqrt(a));
    }
}
