package ru.nsu.kazak.calc.exception;

public class SmallStackException extends NoCriticalException{
    public SmallStackException(Integer minSize, Integer realSize) {
        super("Invalid size of stack. Min size: " + minSize.toString() + ". Real size: " + realSize.toString());
    }
}
