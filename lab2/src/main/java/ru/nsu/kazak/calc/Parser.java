package ru.nsu.kazak.calc;

import ru.nsu.kazak.calc.exception.*;

public class Parser {
    CommandFactory factory;
    public Parser() throws ConfigurationFileException {
        factory = new CommandFactory();
    }
    String[] getWords(String line) {
        return line.split(" ");
    }

    PairCommandArguments parse(String line) throws Throwable {
        if (line.isEmpty()) {
            throw new EmptyLineException();
        }
        String[] words = getWords(line);
        String commandName;
        if (words.length != 0) {
            commandName = words[0];
        }
        else {
            throw new EmptyLineException();
        }
        String[] args = new String[words.length - 1];
        System.arraycopy(words, 1, args, 0, words.length - 1);
        Arguments arguments = new Arguments(args);
        return new PairCommandArguments(factory.create(commandName), arguments);
    }
}
