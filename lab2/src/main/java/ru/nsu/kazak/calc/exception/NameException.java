package ru.nsu.kazak.calc.exception;

public class NameException extends NoCriticalException {
    public NameException(String nameVar) {
        super("Variable's name \"" + nameVar + "\" is unknown");
    }
}
