package ru.nsu.kazak.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.kazak.calc.exception.*;

public class Division extends Command {
    private static final Logger logger = LogManager.getLogger();

    public void process(Arguments argv, Context context) {
        if (argv.numArgs() != 0) {
            throw new NumArgsException(0, argv.numArgs());
        }
        if (badContext(context)) {
            throw new SmallStackException(minSizeStack(), context.getSizeStack());
        }
        Double a = context.getNum();
        context.delNum();
        Double b = context.getNum();
        context.delNum();
        logger.debug("Operation: " + a.toString() + "/" + b.toString());
        if (b == 0) {
            context.addNum(b);
            context.addNum(a);
            throw new ZeroDivizionException();
        }
        context.addNum(a / b);
    }

    protected int minSizeStack() {
        return 2;
    }
}
