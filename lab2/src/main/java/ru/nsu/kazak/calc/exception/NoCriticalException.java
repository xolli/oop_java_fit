package ru.nsu.kazak.calc.exception;

public class NoCriticalException extends RuntimeException {
    public NoCriticalException(String description) { super(description); }
}
