package ru.nsu.kazak.calc.exception;

public class EmptyStackException extends NoCriticalException {
    public EmptyStackException() {
        super("Empty stack");
    }
}
