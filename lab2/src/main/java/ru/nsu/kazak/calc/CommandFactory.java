package ru.nsu.kazak.calc;
import ru.nsu.kazak.calc.exception.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class CommandFactory {
    private Map<String, String> commandClassName;
    private Class clazz;

    public CommandFactory() throws ConfigurationFileException {
        commandClassName = getClasses();
    }

    private Map<String, String> getClasses() throws ConfigurationFileException {
        InputStream fin = getClass().getResourceAsStream("NameClassesCommand.info");
        if (fin == null) {
            throw new ConfigurationFileException();
        }
        int available = 0;
        byte[] buffer;
        try {
            available = fin.available();
            buffer = new byte[available];
            fin.read(buffer, 0, available);
        }
        catch (IOException e) {
            throw new ConfigurationFileException();
        }
        HashMap<String, String> result = new HashMap<String, String>();
        StringBuilder key = new StringBuilder();
        StringBuilder value = new StringBuilder();
        for (int i = 0; i < buffer.length; ++i) {
            while (i < buffer.length && buffer[i] != ' ') {
                key.append((char)buffer[i]);
                ++i;
            }
            ++i;
            while (i < buffer.length && buffer[i] != '\n') {
                value.append((char)buffer[i]);
                ++i;
            }
            result.put(key.toString(), value.toString());
            key = new StringBuilder("");
            value = new StringBuilder("");
        }
    return result;
    }

    public Command create(String commandName) throws Exception {
        try {
            String className = commandClassName.get(commandName);
            if (className == null) {
                throw new UnknownCommandException(commandName);
            }
            Class clazz = Class.forName(className);
            Class[] params = {};
            Command result = (Command) clazz.getConstructor(params).newInstance();
            return result;
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            throw new ConfigurationFileException(e);
        }
    }
}
