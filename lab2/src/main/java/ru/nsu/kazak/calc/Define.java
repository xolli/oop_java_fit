package ru.nsu.kazak.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.kazak.calc.exception.*;

public class Define extends Command {
    private static final Logger logger = LogManager.getLogger();

    public void process(Arguments argv, Context context) {
        if (argv.numArgs() != 2) {
            throw new NumArgsException(2, argv.numArgs());
        }
        logger.debug("Operation: : DEFINE " + argv.getArg(0) + " " + argv.getArg(1));
        try {
            Double.parseDouble(argv.getArg(0));
            throw new DefineArgumentsException();
        } catch (NumberFormatException e) {}
        try {
            context.addVar(argv.getArg(0), Double.parseDouble(argv.getArg(1)));
        } catch (NumberFormatException e) {
            throw new DefineArgumentsException();
        }
    }
}
