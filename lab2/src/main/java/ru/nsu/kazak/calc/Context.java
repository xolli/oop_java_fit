package ru.nsu.kazak.calc;

import ru.nsu.kazak.calc.exception.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Context {
    Map<String, Double> variables;
    Vector<Double> stack;
    public Context () {
        variables = new HashMap<String, Double>();
        stack = new Vector<Double>();
    }
    public Double getNum() {
        if (stack.size() == 0) {
            throw new EmptyStackException();
        }
        return stack.lastElement();
    }
    public void addNum(Double newNum) {
        stack.add(newNum);
    }
    public void addNum(String nameNum) {
        Double addNumber = variables.get(nameNum);
        if (addNumber == null) {
            throw new NameException(nameNum);
        }
        addNum(variables.get(nameNum));
    }
    public void delNum() {
        if (stack.size() == 0) {
            throw new EmptyStackException();
        }
        stack.remove(stack.size() - 1);
    }
    public void addVar(String name, Double value) {
        variables.put(name, value);
    }
    double getVar(String nameVar) {
        Double result = variables.get(nameVar);
        if (result == null) {
            throw new NameException(nameVar);
        }
        return result;
    }
    public int getSizeStack() {
        return stack.size();
    }
}
