package ru.nsu.kazak.calc;

import ru.nsu.kazak.calc.exception.*;

public abstract class Command {
    abstract void process(Arguments argv, Context context);
    protected int minSizeStack() {
        return 0;
    }
    boolean badContext(Context context) {
        return context.getSizeStack() < minSizeStack();
    }
}
