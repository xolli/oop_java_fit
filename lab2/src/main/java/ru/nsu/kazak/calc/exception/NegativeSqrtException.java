package ru.nsu.kazak.calc.exception;

public class NegativeSqrtException extends NoCriticalException {
    public NegativeSqrtException() {
        super("Trying to calculate sqrt from a negative number");
    }
}
