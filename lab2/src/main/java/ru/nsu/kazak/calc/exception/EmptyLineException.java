package ru.nsu.kazak.calc.exception;

public class EmptyLineException extends NoCriticalException {
    public EmptyLineException() {
        super("Empty line");
    }
}
