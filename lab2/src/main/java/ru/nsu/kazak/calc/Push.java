package ru.nsu.kazak.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.kazak.calc.exception.*;

public class Push extends Command {
    private static final Logger logger = LogManager.getLogger();

    private boolean isDouble(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    public void process(Arguments argv, Context context) {
        if (argv.numArgs() != 1) {
            throw new NumArgsException(1, argv.numArgs());
        }
        String argument = argv.getArg(0);
        logger.debug("Operation: PUSH " + argument);
        if (isDouble(argument)) {
            context.addNum(Double.parseDouble(argument));
        }
        else {
            context.addNum(argument);
        }
    }
}
