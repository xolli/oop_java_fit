package ru.nsu.kazak.calc.exception;

public class ConfigurationFileException extends Exception {
    public ConfigurationFileException() {
        super("Error in file \"NameClassesCommand.info\"");
    }
    public ConfigurationFileException(Exception e) { super (e); }
}
