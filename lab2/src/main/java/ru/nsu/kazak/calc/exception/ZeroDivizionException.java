package ru.nsu.kazak.calc.exception;

public class ZeroDivizionException extends NoCriticalException {
    public ZeroDivizionException() {
        super("Division by zero");
    }
}
