package ru.nsu.kazak.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.kazak.calc.exception.*;

public class Pop extends Command {
    private static final Logger logger = LogManager.getLogger();

    public void process(Arguments argv, Context context) {
        logger.debug("Operation: POP");
        if (argv.numArgs() != 0) {
            throw new NumArgsException(0, argv.numArgs());
        }
        context.delNum();
    }
}