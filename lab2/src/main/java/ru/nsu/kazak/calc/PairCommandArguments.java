package ru.nsu.kazak.calc;

public class PairCommandArguments {
    private Command command;
    private Arguments arguments;

    PairCommandArguments(Command comm, Arguments arg) {
        command = comm;
        arguments = arg;
    }
    public Command getCommand() {
        return command;
    }
    public Arguments getArguments() {
        return arguments;
    }
}
