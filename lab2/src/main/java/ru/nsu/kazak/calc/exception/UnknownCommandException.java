package ru.nsu.kazak.calc.exception;

public class UnknownCommandException extends NoCriticalException {
    public UnknownCommandException(String nameCommand) { super("Command's name \"" + nameCommand + "\" is unknown"); }
}
