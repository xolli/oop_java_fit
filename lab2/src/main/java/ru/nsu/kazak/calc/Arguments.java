package ru.nsu.kazak.calc;

public class Arguments {
    String[] argv;
    public Arguments(String[] arguments) {
        argv = new String[arguments.length];
        System.arraycopy(arguments, 0, argv, 0, arguments.length);
    }
    public int numArgs() {
        return argv.length;
    }
    public String getArg(int index) {
        return argv[index];
    }
}
