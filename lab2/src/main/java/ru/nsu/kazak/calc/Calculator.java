package ru.nsu.kazak.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.kazak.calc.exception.*;

import java.util.Scanner;

public class Calculator {
    private static final Logger logger = LogManager.getLogger();
    public void start(Scanner input) throws Throwable {
        Context context = new Context();
        Parser parser = new Parser();
        String line;
        while (input.hasNextLine()) {
            line = input.nextLine();
            try {
                PairCommandArguments pair = parser.parse(line);
                pair.getCommand().process(pair.getArguments(), context);
            } catch (NoCriticalException e) {
                System.out.println(e.getMessage());
                logger.warn(e.getMessage());
            }
        }
    }
}
