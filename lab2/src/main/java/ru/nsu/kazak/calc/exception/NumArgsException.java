package ru.nsu.kazak.calc.exception;

public class NumArgsException extends NoCriticalException {
    public NumArgsException(Integer expected, Integer received) {
        super("Invalid number of arguments. Expected: " + expected.toString() + ". Received: " + received.toString());
    }
}
