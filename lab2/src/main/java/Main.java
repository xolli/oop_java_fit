import ru.nsu.kazak.calc.*;

import java.util.Scanner;
import org.apache.logging.log4j.*;

public class Main {
    private static final Logger logger = LogManager.getLogger();
    public static void main (String[] args) throws Throwable {
        logger.info("Main started");
        Calculator calc = new Calculator();
        Scanner in;
        if (args.length == 0) {
            in = new Scanner(System.in);
        }
        else {
            in = new Scanner(args[0]);
            logger.debug("Open file: " + args[0]);
        }
        try {
           calc.start(in);
        } catch (Exception e) {
            logger.fatal("Error message: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Main finished");
    }
}
