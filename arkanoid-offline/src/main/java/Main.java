import ru.nsu.fit.kazak.arkanoid.*;
import ru.nsu.fit.kazak.arkanoid.exceptions.NoSettingsFile;

public final class Main {
    public static void main(String[] args) {
        Game game;
        try {
            game = new Game();
        } catch (NoSettingsFile e) {
            System.out.println(e.getMessage());
            return;
        }
        game.run();
    }
}
