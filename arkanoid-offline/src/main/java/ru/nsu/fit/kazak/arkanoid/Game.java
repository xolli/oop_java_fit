package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NoLevelFile;
import ru.nsu.fit.kazak.arkanoid.exceptions.NoSettingsFile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class Game extends JFrame {

    private Player player;
    private Ball ball;
    private static final String MOVE_LEFT = "MOVE_LEFT";
    private static final String MOVE_RIGHT = "MOVE_RIGHT";
    private static final String UNTIE = "UNTIE";

    public Game() throws NoSettingsFile {
        super("Arkanoid");
        setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
        setSize(new Dimension(600, 800));
        setResizable(false);
        ball = new Ball(0, 0, 10, Color.PINK);
        player = new Player(this.getWidth(), this.getHeight());
        loadOptions();
        add(player);
    }

    private void loadOptions() throws NoSettingsFile {
        Properties keysTable = new Properties();
        String settingsFile = "options.txt";
        InputStream fin = getClass().getResourceAsStream(settingsFile);
        try {
            keysTable.load(fin);
        } catch (Exception e) {
            throw new NoSettingsFile(settingsFile);
        }
        Set actions = keysTable.keySet();
        for (Object action : actions) {
            player.inputMapPut(keysTable.get(action).toString(), action.toString());
        }
        player.actionMapPut(MOVE_RIGHT, new MoveAction(Side.RIGHT));
        player.actionMapPut(MOVE_LEFT, new MoveAction(Side.LEFT));
        player.actionMapPut(UNTIE, new UntieAction());
    }

    public void run() {
        addMouseMotionListener(player);
        addMouseListener(player);
        player.attachBall(ball);
        Field field;
        try {
            field = new Field(ball, player, this.getWidth(), this.getHeight(), 1);
        } catch (NoLevelFile e) {
            System.out.println(e.getMessage());
            return;
        }
        add(field);
        Timer timer = new Timer(60000, ball);
        timer.start();
        setVisible(true);
        while (true) {
            State state = field.nextFrame();
            if (state == State.GAMEOVER) {
                gameover();
                break;
            } else if (state == State.WIN) {
                win();
                break;
            }
            try {
                Thread.sleep(2);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        timer.stop();
    }

    private void gameover() {
        GameOverWindow gameOver = new GameOverWindow();
        gameOver.showWindow();
    }

    private void win() {
        WinWindow win = new WinWindow();
        win.showWindow();
    }

    private class MoveAction extends AbstractAction {
        Side side;

        MoveAction(Side side) {
            this.side = side;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            player.move(side);
        }
    }

    private class UntieAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            player.untieBound();
        }
    }
}
