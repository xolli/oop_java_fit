package ru.nsu.fit.kazak.arkanoid;
import javax.swing.*;
import java.awt.Color;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;

import java.awt.*;

public class Player extends Sprite implements MouseMotionListener, MouseListener {
    private static final int IFW = JComponent.WHEN_IN_FOCUSED_WINDOW;
    private int widthPlayer, heightPlayer;
    private int widthScreen, heightScreen;
    private boolean isAttachedBall;
    private Ball attachedBall;
    public Player(int setWidthScreen, int setHeightScreen) {
        isAttachedBall = false;
        widthPlayer = 70;
        heightPlayer = 20;
        widthScreen = setWidthScreen;
        heightScreen = setHeightScreen;
        pos = new Coord(0, 0);
        figure = new Rectangle2D.Double(widthScreen / 2, heightScreen * 9 / 10, widthPlayer, heightPlayer);
        color = Color.GREEN;
    }

    public void attachBall(Ball ball) {
        isAttachedBall = true;
        attachedBall = ball;
        moveStickedBall();
    }

    public void goRight() {
        double offset = 10.;
        if (((Rectangle.Double)figure).x + widthPlayer + offset <= widthScreen) {
            ((Rectangle.Double)figure).x += offset;
        }
    }
    public void goLeft() {
        double offset = 10.;
        if (((Rectangle.Double)figure).x  - offset >= 0) {
            ((Rectangle.Double)figure).x -= offset;
        }
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if (mouseEvent.getX() + attachedBall.getDiameter() / 2 < widthScreen && mouseEvent.getX() - attachedBall.getDiameter() / 2 > 0) {
            ((Rectangle.Double)figure).x = mouseEvent.getX() - (double)widthPlayer / 2;
        }
        if (isAttachedBall) {
            moveStickedBall();
        }
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {}

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {}

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.getX() + attachedBall.getDiameter() / 2 < widthScreen && mouseEvent.getX() - attachedBall.getDiameter() / 2 > 0) {
            ((Rectangle.Double)figure).x = mouseEvent.getX() - (double)widthPlayer / 2;
        }
        if (isAttachedBall) {
            moveStickedBall();
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {}

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {}

    @Override
    public void mouseExited(MouseEvent mouseEvent) {}

    public void move(Side side) {
        if (side == Side.LEFT) {
            goLeft();
        } else if (side == Side.RIGHT) {
            goRight();
        }
        if (isAttachedBall) {
            moveStickedBall();
        }
    }

    public void untieBound() {
        if (isAttachedBall) {
            isAttachedBall = false;
            attachedBall.setSpeed(0.75);
        }
    }

    private void moveStickedBall() {
        double myX = ((Rectangle2D.Double) figure).x;
        double myY = ((Rectangle2D.Double) figure).y;
        attachedBall.setPos(myX + widthPlayer / 2 - attachedBall.getDiameter() / 2, myY - attachedBall.getDiameter());
        attachedBall.setSpeed(0.);
    }

    public void inputMapPut(String keyString, String action) {
        getInputMap(IFW).put(KeyStroke.getKeyStroke(keyString), action);
    }

    public void actionMapPut(String actionString, AbstractAction action) {
        getActionMap().put(actionString, action);
    }
}
