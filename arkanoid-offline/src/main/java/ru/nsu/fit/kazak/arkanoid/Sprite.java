package ru.nsu.fit.kazak.arkanoid;

import javax.swing.*;
import java.awt.Color;
import java.awt.geom.Area;
import java.awt.*;

public class Sprite extends JComponent {
    protected Coord pos;
    protected Shape figure;
    protected Color color;
    // https://stackoverflow.com/questions/15690846/java-collision-detection-between-two-shape-objects
    boolean cross(Sprite otherSprite) {
        Area areaA = new Area(figure);
        areaA.intersect(new Area(otherSprite.figure));
        return !areaA.isEmpty();
    }

    public Coord getPos() {
        return pos;
    }

    public void paint(Graphics2D g2) {
        g2.setPaint(color);
        g2.fill(figure);
        g2.draw(figure);
    }
}
