package ru.nsu.fit.kazak.arkanoid;

import javax.swing.*;
import java.awt.*;

public class GameOverWindow extends JFrame {
    GameOverWindow() {
        super("Game Over =(");
        JLabel text = new JLabel("You lose =(");
        add(text);
        setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
        Dimension d = new Dimension(200, 100);
        setSize(d);
        setResizable(false);
    }

    public void showWindow() {
        setVisible(true);
    }
}
