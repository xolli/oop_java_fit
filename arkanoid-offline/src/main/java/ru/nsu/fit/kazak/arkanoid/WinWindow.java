package ru.nsu.fit.kazak.arkanoid;

import javax.swing.*;
import java.awt.*;

public class WinWindow extends JFrame {
    WinWindow() {
        super("You win! =)");
        JLabel text = new JLabel("You win! =)");
        add(text);
        setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
        Dimension d = new Dimension(200, 100);
        setSize(d);
        setResizable(false);
    }

    public void showWindow() {
        setVisible(true);
    }
}
