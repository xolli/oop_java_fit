package ru.nsu.fit.kazak.arkanoid;

import ru.nsu.fit.kazak.arkanoid.exceptions.NoLevelFile;

import javax.swing.*;
import java.awt.*;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

public class Field extends JPanel {
    private Set<Block> blocks;
    private Vector<Ball> balls;
    private Size size;
    private Player player;
    private Integer level;

    public Field(Ball ball, Player player, int width, int height, int level) throws NoLevelFile {
        this.balls = new Vector<Ball>();
        this.blocks = new HashSet<Block>();
        balls.add(ball);
        this.player = player;
        size = new Size(width, height);
        this.level = level;
        buildBlocks();
    }

    private void buildBlocks() throws NoLevelFile {
        Properties blocksTable = new Properties();
        String levelFileName = "level" + level + ".lvl";
        InputStream fin = getClass().getResourceAsStream(levelFileName);
        try {
            blocksTable.load(fin);
        } catch (Exception e) {
            throw new NoLevelFile(levelFileName);
        }
        Set keys = blocksTable.keySet();
        for (Object key : keys) {
            String[] coord = key.toString().split(",", 2);
            int x = Integer.parseInt(coord[0]);
            int y = Integer.parseInt(coord[1]);
            Block newBlock = new Block(new Coord(x, y));
            blocks.add(newBlock);
        }
    }

    public State nextFrame() {
        for (Ball ball : balls) {
            ball.move();
            if ((ball.getPos().y < 0 && ball.moveUp())) {
                ball.push(DirectionSurface.HORIZONT);
            }
            if (ball.getPos().y + ball.getDiameter() > size.getHeight() && ball.moveDown()) {
                repaint();
                return State.GAMEOVER;
            }
            if ((ball.getPos().x < 0 && ball.moveLeft()) || (ball.getPos().x + ball.getDiameter() > size.getWidth() && ball.moveRight())) {
                ball.push(DirectionSurface.VERTICAL);
            }
            if (player.cross(ball) && ball.moveDown()) {
                ball.push(DirectionSurface.HORIZONT);
            }
            for (Block block : blocks) {
                if (block.cross(ball)) {
                    Coord boundPos = ball.getPos();
                    Coord blockPos = block.getPos();
                    Size blockSize = block.getSizeBlock();
                    if (boundPos.x < blockPos.x ||(blockPos.x + blockSize.getWidth() < boundPos.x + 2)) {
                        ball.push(DirectionSurface.VERTICAL);
                    } else {
                        ball.push(DirectionSurface.HORIZONT);
                    }
                    blocks.remove(block);
                    break;
                }
            }
        }
        if (blocks.isEmpty()) {
            repaint();
            return State.WIN;
        }
        repaint();
        return State.GAME;
    }

    @Override
    // https://stackoverflow.com/questions/33784268/fill-a-shape-with-a-random-color-in-java
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        player.paint(g2);
        for (Ball ball : balls) {
            ball.paint(g2);
        }
        for (Block block : blocks) {
            block.paint(g2);
        }
    }
}
