package ru.nsu.fit.kazak.arkanoid.exceptions;

public class NoLevelFile extends Exception {
    public NoLevelFile(String levelFileName) {
        super("No level with name: " + levelFileName);
    }
}
