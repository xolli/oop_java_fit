package ru.nsu.fit.kazak.arkanoid;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;

public class Ball extends Sprite implements ActionListener {
    private int diameter;
    private Double direction;
    private Double speed;

    public Ball(int x, int y, int diameter, Color color) {
        direction = Math.PI / 2;
        speed = 0.;
        this.diameter = diameter;
        this.color = color;
        figure = new Ellipse2D.Double(x, y, diameter, diameter);
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getSpeed() {
        return speed;
    }

    public void move() {
        Double dX = Math.cos(direction) * speed;
        Double dY = -Math.sin(direction) * speed;
        ((Ellipse2D.Double)figure).x += dX;
        ((Ellipse2D.Double)figure).y += dY;
    }

    private Double getRandomDouble(Double min, Double max) {
        return (Math.random()*((max - min))) + min;
    }

    public void push (DirectionSurface directionSurface) {
        Double delta = Math.PI / 15;
        if (directionSurface == DirectionSurface.HORIZONT && direction >= 0 && direction <= Math.PI / 2) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.HORIZONT && direction > Math.PI / 2 && direction <= Math.PI) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.HORIZONT && direction > Math.PI && direction <= Math.PI * 3 / 2) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.HORIZONT && direction > Math.PI * 3 / 2 && direction <= Math.PI * 2) {
            direction = 2 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.VERTICAL && direction >= Math.PI / 2 && direction <= Math.PI) {
            direction = Math.PI - direction;
        }  else if (directionSurface == DirectionSurface.VERTICAL && direction >= Math.PI && direction <= Math.PI * 3 / 2) {
            direction = 3 * Math.PI - direction;
        } else if (directionSurface == DirectionSurface.VERTICAL && direction <= Math.PI / 2) {
            direction = Math.PI - direction;
        } else if (directionSurface == DirectionSurface.VERTICAL && direction >= Math.PI * 3 / 2) {
            direction = Math.PI * 3 / 2 - Math.abs(Math.PI * 3 / 2 - direction);
        }
        direction += getRandomDouble(-delta, delta);
        direction %= Math.PI * 2;
        if (direction < 0) {
            direction += Math.PI * 2;
        }
    }

    public Coord getPos() {
        return new Coord((int)((Ellipse2D.Double)figure).x, (int) ((Ellipse2D.Double)figure).y);
    }

    public void setPos(Double x, Double y) {
        ((Ellipse2D.Double)figure).x = x;
        ((Ellipse2D.Double)figure).y = y;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public boolean moveUp() {
        return direction < Math.PI;
    }

    public boolean moveDown() {
        return !moveUp();
    }

    public boolean moveLeft() {
        return direction > Math.PI / 2 && direction < Math.PI * 3 / 2;
    }

    public boolean moveRight() {
        return !moveLeft();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        speed += 0.5;
    }
}
