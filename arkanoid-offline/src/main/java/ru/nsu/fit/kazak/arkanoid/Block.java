package ru.nsu.fit.kazak.arkanoid;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Block extends Sprite {
    private Size size;
    private Integer getRandomInt(Integer min, Integer max) {
        return (int) (min + Math.random() * (max - min + 1));
    }

    public Block(Coord pos) {
        size = new Size(50, 20);
        this.pos = pos;
        figure = new Rectangle2D.Double(pos.x, pos.y, size.getWidth(), size.getHeight());
        Color[] colors = {Color.RED, Color.GREEN, Color.BLUE, Color.PINK, Color.CYAN, Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.YELLOW};
        color = colors[getRandomInt(0, colors.length - 1)];
    }

    public Size getSizeBlock() {
        return size;
    }
}
